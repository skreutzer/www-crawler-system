<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/configure_crawl.php
 * @author Stephan Kreutzer
 * @since 2023-03-20
 */


require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");

require_once("./libraries/user_management.inc.php");


if (((int)($_SESSION['user_role'])) !== USER_ROLE_ADMIN)
{
    http_response_code(403);
    exit(1);
}

if (isset($_GET["bucket-id"]) !== true)
{
    http_response_code(400);
    echo "'bucket-id' is missing.";
    exit(0);
}

$bucketId = (int)$_GET["bucket-id"];

require_once("./libraries/bucket_management.inc.php");

$bucket = getBucketById($bucketId);

if (is_array($bucket) !== true)
{
    http_response_code(500);
    exit(1);
}

if (count($bucket) < 0)
{
    http_response_code(404);
    exit(0);
}

$bucket = $bucket[0];

if (isset($_POST["update-bucket-user-mapping"]) === true)
{
    $mapping = array();

    if (isset($_POST["user-mapping"]) === true)
    {
        $mapping = $_POST["user-mapping"];

        for ($i = 0, $max = count($mapping); $i < $max; $i++)
        {
            $mapping[$i] = (int)$mapping[$i];
        }
    }

    $result = updateBucketUserMapping($bucketId, $mapping);

    if ($result !== 0)
    {
        http_response_code(500);
        exit(0);
    }
}
else if (isset($_POST["update-threshold"]) === true)
{
    if (isset($_POST["threshold"]) !== true)
    {
        http_response_code(400);
        echo "'threshold' is missing.";
        exit(0);
    }

    $filterId = (int)$_POST["update-threshold"];
    $threshold = (int)$_POST["threshold"];

    $result = updateBucketUrlCountFilters($bucketId, $filterId, $threshold);

    if ($result !== 0)
    {
        http_response_code(500);
        exit(1);
    }
}
else if (isset($_POST["add-threshold"]) === true)
{
    if (isset($_POST["url"]) !== true)
    {
        http_response_code(400);
        echo "'url' is missing.";
        exit(0);
    }

    if (isset($_POST["threshold"]) !== true)
    {
        http_response_code(400);
        echo "'threshold' is missing.";
        exit(0);
    }

    $result = insertBucketUrlCountFilters($bucketId, $_POST["url"], (int)$_POST["threshold"]);

    if ($result <= 0)
    {
        http_response_code(500);
        exit(1);
    }
}

$bucketUserMapping = getBucketUserMapping($bucketId);

if (is_array($bucketUserMapping) !== true)
{
    http_response_code(500);
    exit(0);
}

{
    $temp = array();

    for ($i = 0, $max = count($bucketUserMapping); $i < $max; $i++)
    {
        $temp[] = (int)$bucketUserMapping[$i]["id_user"];
    }

    $bucketUserMapping = $temp;
}

$users = getUsers();

if (is_array($users) !== true)
{
    http_response_code(500);
    exit(0);
}

$countFilters = getBucketUrlCountFilters($bucketId);

if (is_array($countFilters) !== true)
{
    http_response_code(500);
    exit(0);
}


require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("configure_crawl"));


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n".
     "        <h2>".LANG_HEADER_CRAWLFORSTARTURL.htmlspecialchars($bucket["url"], ENT_XHTML, "UTF-8")."</h2>\n".
     "        <form method=\"post\" action=\"configure_crawl.php?bucket-id=".$bucketId."\">\n".
     "          <fieldset>\n".
     "            <table>\n".
     "              <thead>\n".
     "                <tr>\n".
     "                  <th>".LANG_TABLEHEADER_USERNAME."</th>\n".
     "                  <th>".LANG_TABLEHEADER_SELECT."</th>\n".
     "                </tr>\n".
     "              </thead>\n".
     "              <tfoot>\n".
     "                <tr>\n".
     "                  <td colspan=\"2\">\n".
     "                    <input type=\"submit\" name=\"update-bucket-user-mapping\" value=\"".LANG_FORMLABEL_UPDATE."\"/>\n".
     "                  </td>\n".
     "                </tr>\n".
     "              </tfoot>\n".
     "              <tbody>\n";

for ($i = 0, $max = count($users); $i < $max; $i++)
{
    $userId = (int)$users[$i]["id"];

    echo "                <tr>\n".
         "                  <td>".htmlspecialchars($users[$i]["name"], ENT_XHTML, "UTF-8")."</td>\n".
         "                  <td><input type=\"checkbox\" name=\"user-mapping[]\" value=\"".$userId."\"";

    if (in_array($userId, $bucketUserMapping) === true)
    {
        echo " checked=\"checked\"";
    }

    echo "/></td>\n".
         "                </tr>\n";
}

echo "              </tbody>\n".
     "            </table>\n".
     "          </fieldset>\n".
     "        </form>\n".
     "        <hr/>\n".
     "        <table>\n".
     "          <thead>\n".
     "            <tr>\n".
     "              <th>".LANG_TABLEHEADER_URL."</th>\n".
     "              <th>".LANG_TABLEHEADER_THRESHOLD."</th>\n".
     "            </tr>\n".
     "          </thead>\n".
     "          <tfoot>\n".
     "            <tr>\n".
     "              <td colspan=\"2\">\n".
     "                <form method=\"post\" action=\"configure_crawl.php?bucket-id=".$bucketId."\">\n".
     "                  <fieldset>\n".
     "                    <label for=\"field-url\">".LANG_FORMLABEL_URL."</label>\n".
     "                    <input type=\"text\" name=\"url\" id=\"field-url\" required=\"required\"/>\n".
     "                    <label for=\"field-threshold\">".LANG_FORMLABEL_THRESHOLD."</label>\n".
     "                    <input type=\"text\" name=\"threshold\" id=\"field-threshold\" required=\"required\"/>\n".
     "                    <input type=\"submit\" name=\"add-threshold\" value=\"".LANG_BUTTONCAPTION_ADD."\"/>\n".
     "                  </fieldset>\n".
     "                </form>\n".
     "              </td>\n".
     "            </tr>\n".
     "          </tfoot>\n".
     "          <tbody>\n";

for ($i = 0, $max = count($countFilters); $i < $max; $i++)
{
    $filterId = (int)$countFilters[$i]["url_count_filter_count_id"];

    echo "            <tr>\n".
         "              <td>".htmlspecialchars($countFilters[$i]["resource_url"], ENT_XHTML, "UTF-8")."</td>\n".
         "              <td>\n".
         "                <form method=\"post\" action=\"configure_crawl.php?bucket-id=".$bucketId."\">\n".
         "                  <fieldset>\n".
         "                    <input type=\"text\" name=\"threshold\" value=\"".((int)$countFilters[$i]["url_count_filter_count_threshold"])."\"/> ".
         "<button type=\"submit\" name=\"update-threshold\" value=\"".$filterId."\" required=\"required\">".LANG_FORMLABEL_UPDATE."</button>\n".
         "                  </fieldset>\n".
         "                </form>\n".
         "              </td>\n".
         "            </tr>\n";
}

echo "          </tbody>\n".
     "        </table>\n".
     "        <hr/>\n".
     "        <div>\n".
     "          <a href=\"manage_crawls.php\">".LANG_LINKCAPTION_CRAWLSPAGE."</a><br/>\n".
     "          <a href=\"index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
     "        </div>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>

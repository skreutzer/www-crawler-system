<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2023-03-10
 */


require_once("../libraries/https.inc.php");

if (isset($_GET["bucket-id"]) !== true)
{
    http_response_code(400);
    echo "'bucket-id' is missing.";
    exit(0);
}

if (isset($_GET["resource-handle"]) !== true)
{
    http_response_code(400);
    echo "'resource-handle' is missing.";
    exit(0);
}

require_once("../libraries/session.inc.php");
require_once("../libraries/user_management.inc.php");

if (((int)($_SESSION["user_role"])) !== USER_ROLE_ADMIN)
{
    http_response_code(403);
    exit(0);
}


require_once("./CustomHtmlReader.inc.php");

/** @todo Check or sanitize. */
$bucketId = (int)$_GET["bucket-id"];
/** @todo Check or sanitize. */
$resourceHandle = $_GET["resource-handle"];

$sourcePath = "./storage/".$bucketId."/".$resourceHandle;

if (file_exists($sourcePath) !== true)
{
    /** @todo Error handling. */
}

// Or inherit from StreamInterface to have generic
// streams on both, string and file sources.

$input = file_get_contents($sourcePath);

if ($input === false)
{
    /** @todo Error handling. */
}

$reader = new CustomHtmlReader($input);
$reader->extract();
$errors = $reader->getErrors();
$errorsCount = count($errors);

if ($errorsCount > 0)
{
    $errorPath = "./storage/".$bucketId."/".$resourceHandle.".err.json";

    if (file_exists($errorPath) !== false)
    {
        http_response_code(500);
        exit(1);
    }

    $writer = fopen($errorPath, "w");

    if ($writer == false)
    {
        http_response_code(500);
        exit(-1);
    }

    if (fwrite($writer, "{\"errors\":[") === false)
    {
        http_response_code(500);
        exit(-1);
    }

    for ($i = 0; $i < $errorsCount; $i++)
    {
        if ($i > 0)
        {
            if (fwrite($writer, ",") === false)
            {
                http_response_code(500);
                exit(-1);
            }
        }

        if (fwrite($writer, "{\"code\":".((int)$errors[$i]["code"]).",\"buffer\":".json_encode($errors[$i]["buffer"])."}") === false)
        {
            http_response_code(500);
            exit(-1);
        }
    }

    if (fwrite($writer, "]}") === false)
    {
        http_response_code(500);
        exit(-1);
    }

    if (fclose($writer) === false)
    {
        http_response_code(500);
        exit(-1);
    }
}

$links = $reader->getLinks();

// Worked when the links were in the array as values, but
// that's no longer the case...
//$links = array_values(array_unique($links, SORT_STRING));

{
    $linksUnique = array();

    for ($i = 0, $max = count($links); $i < $max; $i++)
    {
        $link = $links[$i]["url"];

        if (array_key_exists($link, $linksUnique) !== true)
        {
            $linksUnique[$link] = array();
        }

        if (in_array((int)$links[$i]["context"], $linksUnique[$link]) !== true)
        {
            $linksUnique[$link][] = (int)$links[$i]["context"];
        }
    }

    $links = $linksUnique;
}


http_response_code(200);
header("Content-Type: application/json");

echo "{\"links\":[";

if (count($links) <= 0)
{
    echo "]}";
    exit(0);
}

$first = true;

foreach ($links as $link => $contexts)
{
    /*
    if (stripos($link, "https://") !== 0 &&
        stripos($link, "http://") !== 0)
    {
        continue;
    }
    */

    if ($first == true)
    {
        $first = false;
    }
    else
    {
        echo ",";
    }

    echo "{\"url\":".json_encode($link).",\"contexts\":[";

    for ($i = 0, $max = count($contexts); $i < $max; $i++)
    {
        if ($i > 0)
        {
            echo ",";
        }

        echo (int)$contexts[$i];
    }

    echo "]}";
}

echo "],\"hasErrors\":";

if ($errorsCount > 0)
{
    echo "true";
}
else
{
    echo "false";
}

echo "}";


?>

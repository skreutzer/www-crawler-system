<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @brief Only extracts links from HTML pages.
 * @details This variant additionally records the context in which a link
 *     was found.
 * @since 2023-03-10
 */


require_once(dirname(__FILE__)."/../libraries/resource_defines.inc.php");


define("CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS", 1);
define("CUSTOMHTMLREADER_ERRORCODE_EMPTYATTRIBUTENAME", 2);
define("CUSTOMHTMLREADER_ERRORCODE_NOASSIGNMENTAFTERATTRIBUTENAME", 3);
define("CUSTOMHTMLREADER_ERRORCODE_NOATTRIBUTEVALUEDELIMITER", 4);
define("CUSTOMHTMLREADER_ERRORCODE_ATTRIBUTEHREFDUPLICATE", 5);
define("CUSTOMHTMLREADER_ERRORCODE_EMPTYANCHOR", 6);
define("CUSTOMHTMLREADER_ERRORCODE_LINKWITHOUTCAPTION", 7);

// Typical use: abort all further parsing, because end of file was
// reached, while within some element, not a legal/valid end reached!
define("CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT", -2);
// Typical use: Proper parse was in progress, but turned out construct
// is irrelevant/un-interesting, so get up/out of it and proceed with
// the next newly encountered start of a sequence that might be relevant.
define("CUSTOMHTMLREADER_RETURNCODE_IRRELEVANT", 1);
// Parsing for the local part successfully completed.
define("CUSTOMHTMLREADER_RETURNCODE_COMPLETED", 0);


class CustomHtmlReader
{
    function __construct(&$input)
    {
        $this->input = &$input;
        $this->max = strlen($this->input);
    }

    public function extract()
    {
        while ($this->cursor < $this->max)
        {
            if ($this->input[$this->cursor] == "<")
            {
                $this->cursor++;

                $result = $this->handleTag();

                if ($result == CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT)
                {
                    return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
                }
            }
            else if ($this->input[$this->cursor] == "-")
            {
                if ($this->inComment == true)
                {
                    $result = $this->handleCommentEnd();

                    /** @todo Handle result. */
                }
                else
                {
                    $this->cursor++;
                }
            }
            else
            {
                $this->cursor++;
            }
        }

        return 0;
    }

    protected function handleTag()
    {
        if ($this->cursor >= $this->max)
        {
            return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
        }

        if ($this->input[$this->cursor] == "/")
        {
            $this->cursor++;

            $result = $this->handleEndElement();

            /** @todo Handle $result. */
            return $result;
        }
        else if ($this->input[$this->cursor] == "!")
        {
            $result = $this->handleMarkupDeclaration();

            /** @todo Handle $result. */
            return $result;
        }
        else if ($this->input[$this->cursor] == "?")
        {
/** @todo Handle! */
            return 0;
        }
        else
        {
            $result = $this->handleStartElement();

            if ($result === CUSTOMHTMLREADER_RETURNCODE_COMPLETED)
            {
                return CUSTOMHTMLREADER_RETURNCODE_COMPLETED;
            }
            else if ($result === CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT)
            {
                return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
            }
            else
            {
                return -1;
            }
        }

        throw new Exception("Not implemented.");
    }

    protected function handleMarkupDeclaration()
    {
        $this->cursor++;

        if ($this->cursor >= $this->max)
        {
            return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
        }

        if ($this->input[$this->cursor] != "-")
        {
            $this->cursor++;
            return CUSTOMHTMLREADER_RETURNCODE_IRRELEVANT;
        }

        $this->cursor++;

        if ($this->cursor >= $this->max)
        {
            return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
        }

        if ($this->input[$this->cursor] != "-")
        {
            $this->cursor++;
            return CUSTOMHTMLREADER_RETURNCODE_IRRELEVANT;
        }

        $this->inComment = true;
        $this->cursor++;

        return 0;
    }

    protected function handleCommentEnd()
    {
        $this->cursor++;

        if ($this->cursor >= $this->max)
        {
            return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
        }

        if ($this->input[$this->cursor] != "-")
        {
            $this->cursor++;
            return CUSTOMHTMLREADER_RETURNCODE_IRRELEVANT;
        }

        $this->cursor++;

        if ($this->cursor >= $this->max)
        {
            return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
        }

        if ($this->input[$this->cursor] != ">")
        {
            $this->cursor++;
            return CUSTOMHTMLREADER_RETURNCODE_IRRELEVANT;
        }

        $this->inComment = false;
        $this->cursor++;

        return 0;
    }

    protected function handleStartElement()
    {
        $name = $this->handleElementName();

        if ($name === CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT)
        {
            return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
        }

        if ($name === "a" ||
            $name === "A")
        {
            $result = $this->handleElementA($name);

            if ($result === CUSTOMHTMLREADER_RETURNCODE_COMPLETED)
            {
                $this->buffer = "";

                return CUSTOMHTMLREADER_RETURNCODE_COMPLETED;
            }
            else if ($result === CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT)
            {
                return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
            }
            else
            {
                return -1;
            }
        }
        else if (strcasecmp($name, "script") === 0)
        {
            $this->inScript = true;
        }
        else
        {
            $this->buffer = "";
            return CUSTOMHTMLREADER_RETURNCODE_IRRELEVANT;
        }
    }

    protected function handleEndElement()
    {
        $name = $this->handleElementName();

        if ($name === CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT)
        {
            return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
        }

        if (strcasecmp($name, "script") === 0)
        {
            $this->inScript = false;
        }

        return 0;
    }

    /**
     * @todo Doesn't provide access to partial name in case of error,
     *     to add it to $this->buffer, for example.
     */
    protected function handleElementName()
    {
        $name = "";

        do
        {
            if ($this->cursor >= $this->max)
            {
                return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
            }

            if (ctype_space($this->input[$this->cursor]) === true)
            {
                return $name;
            }
            else if ($this->input[$this->cursor] == ">" ||
                     $this->input[$this->cursor] == "/")
            {
                return $name;
            }
            else
            {
                $name .= $this->input[$this->cursor];
            }

            $this->cursor++;

        } while (true);
    }

    protected function handleElementA($name)
    {
        /** @todo Append or overwrite/reset? $this->buffer
          * should be empty when entering here. */
        $this->buffer .= "<".$name;

        if (ctype_space($this->input[$this->cursor]) !== true)
        {
            $this->errors[] = array("buffer" => $this->buffer.$this->input[$this->cursor], "code" => CUSTOMHTMLREADER_ERRORCODE_EMPTYANCHOR);
            $this->buffer = "";
            return -1;
        }

        $result = $this->handleAttributes();

        if ($result === CUSTOMHTMLREADER_RETURNCODE_COMPLETED)
        {
            return CUSTOMHTMLREADER_RETURNCODE_COMPLETED;
        }
        else if ($result === CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT)
        {
            return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
        }
        else
        {
            return -1;
        }
    }

    protected function handleAttributes()
    {
        $attributeValue = null;
        $first = true;

        do
        {
            if (ctype_space($this->input[$this->cursor]) === true)
            {
                if ($this->consumeWhitespace(true) == CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT)
                {
                    return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
                }
            }

            if ($this->input[$this->cursor] == "/")
            {
                if ($first == true)
                {
                    $this->errors[] = array("buffer" => $this->buffer."/", "code" => CUSTOMHTMLREADER_ERRORCODE_EMPTYANCHOR);
                    $this->buffer = "";
                    return -1;
                }

                if ($attributeValue != null)
                {
                    $this->errors[] = array("buffer" => $this->buffer."/", "code" => CUSTOMHTMLREADER_ERRORCODE_LINKWITHOUTCAPTION);
                    $this->buffer = "";
                    return -1;
                }
            }

            if ($this->input[$this->cursor] == ">")
            {
                $this->buffer .= ">";

                if ($first == true)
                {
                    $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_EMPTYANCHOR);
                    $this->buffer = "";
                    return -1;
                }

                return CUSTOMHTMLREADER_RETURNCODE_COMPLETED;
            }
            else
            {
                $first = false;
            }

            $attributeValueCurrent = $this->handleAttribute();

            if ($attributeValueCurrent === -1)
            {
                return -1;
            }
            else if ($attributeValueCurrent === CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT)
            {
                return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
            }

            if ($attributeValue == null)
            {
                $attributeValue = $attributeValueCurrent;
            }
            else
            {
                if ($attributeValueCurrent !== null)
                {
                    $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_ATTRIBUTEHREFDUPLICATE);
                    $this->buffer = "";
                    return -1;
                }
            }

        } while (true);

        throw new Exception("Not implemented.");
    }

    protected function handleAttribute()
    {
        $attributeName = "";

        do
        {
            if ($this->cursor >= $this->max)
            {
                $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS);
                $this->buffer = "";
                return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
            }

            if ($this->input[$this->cursor] == "=" ||
                ctype_space($this->input[$this->cursor]) === true ||
                // This also accepts invalid attributes (only names without value).
                $this->input[$this->cursor] == ">" ||
                $this->input[$this->cursor] == "<" ||
                $this->input[$this->cursor] == "/")
            {
                break;
            }

            $attributeName .= $this->input[$this->cursor];
            $this->buffer .= $this->input[$this->cursor];
            $this->cursor++;

        } while (true);

        if (strlen($attributeName) <= 0)
        {
            $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_EMPTYATTRIBUTENAME);
            $this->buffer = "";
            return -1;
        }

        if (ctype_space($this->input[$this->cursor]) === true)
        {
            if ($this->consumeWhitespace(true) == CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT)
            {
                return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
            }
        }

        if ($this->input[$this->cursor] == "=")
        {
            $this->buffer .= "=";
            $this->cursor++;

            if ($this->cursor >= $this->max)
            {
                $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS);
                $this->buffer = "";
                return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
            }
        }
        else
        {
            // This also accepts invalid attributes (only names without value).

            if(strcasecmp($attributeName, "href") === 0 ||
               $this->input[$this->cursor] == "<" ||
               $this->input[$this->cursor] == "/")
            {
                $this->buffer .= $this->input[$this->cursor];

                $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_NOASSIGNMENTAFTERATTRIBUTENAME);
                $this->buffer = "";
                return -1;
            }
            else if ($this->input[$this->cursor] == ">")
            {
                return -1;
            }
            else
            {
                return null;
            }
        }

        if (ctype_space($this->input[$this->cursor]) === true)
        {
            if ($this->consumeWhitespace(true) == CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT)
            {
                return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
            }
        }

        $delimiter = $this->input[$this->cursor];
        $this->buffer .= $delimiter;

        if ($delimiter != "'" &&
            $delimiter != "\"")
        {
            $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_NOATTRIBUTEVALUEDELIMITER);
            $this->buffer = "";
            return -1;
        }

        $attributeValue = "";

        do
        {
            $this->cursor++;

            if ($this->cursor >= $this->max)
            {
                $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS);
                $this->buffer = "";
                return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
            }

            if ($this->input[$this->cursor] == $delimiter)
            {
                $this->buffer .= $delimiter;
                $this->cursor++;

                if ($this->cursor >= $this->max)
                {
                    $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS);
                    $this->buffer = "";
                    return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
                }

                break;
            }

            $attributeValue .= $this->input[$this->cursor];
            $this->buffer .= $this->input[$this->cursor];

        } while (true);

        if (strcasecmp($attributeName, "href") === 0 &&
            strlen($attributeValue) > 0)
        {
            $context = RESOURCE_LINKCONTEXT_REGULAR;

            if ($this->inComment == true)
            {
                $context = RESOURCE_LINKCONTEXT_COMMENT;
            }
            else if ($this->inScript == true)
            {
                $context = RESOURCE_LINKCONTEXT_SCRIPT;
            }

            $this->links[] = array("url" => $attributeValue, "context" => $context);
            return $attributeValue;
        }

        return null;
    }

    protected function consumeWhitespace($record)
    {
        while (ctype_space($this->input[$this->cursor]) === true)
        {
            if ($record == true)
            {
                $this->buffer .= $this->input[$this->cursor];
            }

            $this->cursor++;

            if ($this->cursor >= $this->max)
            {
                if ($record == true)
                {
                    $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS);
                    $this->buffer = "";
                }

                return CUSTOMHTMLREADER_RETURNCODE_GLOBALABORT;
            }
        }

        return 0;
    }

    public function getLinks()
    {
        return $this->links;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected $inScript = false;
    protected $inComment = false;
    protected $inProcessingInstruction = false;

    protected $cursor = 0;
    protected $max = 0;
    protected $input;
    protected $links = array();
    protected $buffer = "";
    protected $errors = array();
}

?>

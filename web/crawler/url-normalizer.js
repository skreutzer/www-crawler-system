/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2023-03-14
 */

"use strict";

function normalizeUrl(url, baseUrl)
{
    if (url == 0)
    {
        return -1;
    }

    if (url == "/")
    {
        if (baseUrl != null)
        {
            let newUrl = new URL(baseUrl);
            url = newUrl.origin;
        }
        else
        {
            return -1;
        }
    }

    let fragmentDelimiterPos = url.indexOf("#");

    if (fragmentDelimiterPos >= 0)
    {
        url = url.substring(0, fragmentDelimiterPos);
    }

    if (url.length == 0)
    {
        return 0;
    }

    if (url.indexOf("https://") != 0 &&
        url.indexOf("http://") != 0)
    {
        let delimiterPos = url.indexOf("/");

        if (delimiterPos == 0)
        {
            if (baseUrl != null)
            {
                let newUrl = new URL(url, baseUrl);
                url = newUrl.toString();
            }
            else
            {
                return -1;
            }
        }
        else
        {
            // All of this is stupid. But unfortunately that's how it is
            // with the WWW and its browsers :-(

            // Note: file names can include dots in their name just as well.

            let domainCandidate = null;

            if (delimiterPos > 0)
            {
                domainCandidate = url.substring(0, delimiterPos);
            }
            else
            {
                domainCandidate = url;
            }

            let separatorCount = 0;

            for (let i = 0, max = domainCandidate.length; i < max; i++)
            {
                if (domainCandidate[i] == '.')
                {
                    separatorCount += 1;
                }
            }

            if (separatorCount >= 1)
            {
                url = "http://" + url;
            }
            else
            {
                if (baseUrl != null)
                {
                    let newUrl = new URL(url, baseUrl);
                    url = newUrl.toString();
                }
                else
                {
                    return -1;
                }
            }
        }
    }

    if (baseUrl != null)
    {
        let newUrl = new URL(baseUrl);

        if (url.indexOf(newUrl.origin) == 0)
        {
            let subPart = url.substring(newUrl.origin.length);

            if (subPart == "/")
            {
                url = newUrl.origin;
            }
        }
    }

    return url;
}

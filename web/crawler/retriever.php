<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2023-03-10
 */

require_once("../libraries/https.inc.php");

if (isset($_GET["bucket-id"]) !== true)
{
    http_response_code(400);
    echo "'bucket-id' is missing.";
    exit(0);
}

if (isset($_GET["url"]) !== true)
{
    http_response_code(400);
    echo "'url' is missing.";
    exit(0);
}

require_once("../libraries/session.inc.php");
require_once("../libraries/user_management.inc.php");

if (((int)($_SESSION["user_role"])) !== USER_ROLE_ADMIN)
{
    http_response_code(403);
    exit(0);
}


/** @todo Check or sanitize. */
$bucketId = (int)$_GET["bucket-id"];
/** @todo Check or sanitize. */
$url = $_GET["url"];

if (file_exists("./storage/".$bucketId."/") !== true)
{
    if (mkdir("./storage/".$bucketId."/", 0775) !== true)
    {
        http_response_code(500);
        exit(-1);
    }
}

$resourceHandle = md5(uniqid(rand(), true));
$filePath = "./storage/".$bucketId."/".$resourceHandle;

if (file_exists($filePath) !== false)
{
    http_response_code(500);
    exit(1);
}

$curl = curl_init();

if ($curl === false)
{
    http_response_code(500);
    exit(-1);
}

if (curl_setopt_array($curl,
                      array(CURLOPT_URL => $url,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_HEADER => true,
                            CURLOPT_NOBODY => false,
                            CURLOPT_USERAGENT => "www-crawler-system")) === false)
{
    http_response_code(500);
    exit(-1);
}

$response = curl_exec($curl);

if ($response === false)
{
    http_response_code(500);
    // curl_error($curl);
    exit(-1);
}

$httpResponseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
$headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

curl_close($curl);

$header = substr($response, 0, $headerSize);
$response = substr($response, $headerSize);

if ($httpResponseCode == 200)
{
    if (file_put_contents($filePath, $response) === false)
    {
        http_response_code(500);
        exit(0);
    }
}
else
{
    $resourceHandle = "";
}

http_response_code(200);
header("Content-Type: application/json");
echo "{\"bucketId\":".json_encode($bucketId).",".
      "\"url\":".json_encode($url).",".
      "\"httpResponseCode\":".((int)$httpResponseCode).",".
      "\"resourceHandle\":".json_encode($resourceHandle)."}";


?>

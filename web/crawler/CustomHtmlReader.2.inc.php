<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @brief Only extracts links from HTML pages.
 * @details This variant avoids a "run-away" situation (continue beyond the
 *     end of the element/tag) for ("standalone") attributes without value
 *     assignment, as not well-formed in XHTML/XML, but permitted + common
 *     with HTML4. It will however log an error for the entire a/anchor ("link")
 *     element and not collect a href URL, as the element might be malformed.
 *     Just further parsing of other elements doesn't get sabotaged by such
 *     a scenario any more.
 * @since 2023-03-10
 */

define("CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS", 1);
define("CUSTOMHTMLREADER_ERRORCODE_EMPTYATTRIBUTENAME", 2);
define("CUSTOMHTMLREADER_ERRORCODE_NOASSIGNMENTAFTERATTRIBUTENAME", 3);
define("CUSTOMHTMLREADER_ERRORCODE_NOATTRIBUTEVALUEDELIMITER", 4);
define("CUSTOMHTMLREADER_ERRORCODE_ATTRIBUTEHREFDUPLICATE", 5);
define("CUSTOMHTMLREADER_ERRORCODE_EMPTYANCHOR", 6);

class CustomHtmlReader
{
    function __construct(&$input)
    {
        $this->input = &$input;
        $this->max = strlen($this->input);
    }

    public function extract()
    {
        while ($this->cursor < $this->max)
        {
            if ($this->input[$this->cursor] == "<")
            {
                $this->cursor++;

                $result = $this->handleTag();

                if ($result == -2)
                {
                    return -2;
                }
            }
            else
            {
                $this->cursor++;
            }
        }

        return 0;
    }

    protected function handleTag()
    {
        if ($this->cursor >= $this->max)
        {
            return -1;
        }

        if ($this->input[$this->cursor] != "a" &&
            $this->input[$this->cursor] != "A")
        {
            $this->buffer = "";
            $this->cursor++;
            return 1;
        }
        else
        {
            $this->buffer .= "<".$this->input[$this->cursor];
            $this->cursor++;

            if ($this->cursor >= $this->max)
            {
                $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS);
                $this->buffer = "";
                return -2;
            }

            if (ctype_space($this->input[$this->cursor]) !== true)
            {
                $this->buffer = "";
                $this->cursor++;
                return 1;
            }
        }

        $result = $this->handleAttributes();

        if ($result === 0)
        {
            return 0;
        }
        else if ($result === -2)
        {
            return -2;
        }
        else
        {
            return -1;
        }
    }

    protected function handleAttributes()
    {
        $attributeValue = null;
        $first = true;

        do
        {
            if (ctype_space($this->input[$this->cursor]) === true)
            {
                if ($this->consumeWhitespace(true) == -2)
                {
                    return -2;
                }
            }

            if ($this->input[$this->cursor] == ">")
            {
                $this->buffer .= ">";

                if ($first == true)
                {
                    $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_EMPTYANCHOR);
                    $this->buffer = "";
                    return -1;
                }

                return 0;
            }
            else
            {
                $first = false;
            }

            $attributeValueCurrent = $this->handleAttribute();

            if ($attributeValueCurrent === -1)
            {
                return -1;
            }
            else if ($attributeValueCurrent === -2)
            {
                return -2;
            }

            if ($attributeValue == null)
            {
                $attributeValue = $attributeValueCurrent;
            }
            else
            {
                if ($attributeValueCurrent !== null)
                {
                    $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_ATTRIBUTEHREFDUPLICATE);
                    $this->buffer = "";
                    return -1;
                }
            }

        } while (true);

        return 0;
    }

    protected function handleAttribute()
    {
        $attributeName = "";

        do
        {
            if ($this->cursor >= $this->max)
            {
                $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS);
                $this->buffer = "";
                return -2;
            }

            if ($this->input[$this->cursor] == "=" ||
                ctype_space($this->input[$this->cursor]) === true ||
                // This also accepts invalid attributes (only names without value).
                $this->input[$this->cursor] == ">" ||
                $this->input[$this->cursor] == "<" ||
                $this->input[$this->cursor] == "/")
            {
                break;
            }

            $attributeName .= $this->input[$this->cursor];
            $this->buffer .= $this->input[$this->cursor];
            $this->cursor++;

        } while (true);

        if (strlen($attributeName) <= 0)
        {
            $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_EMPTYATTRIBUTENAME);
            $this->buffer = "";
            return -1;
        }

        if (ctype_space($this->input[$this->cursor]) === true)
        {
            if ($this->consumeWhitespace(true) == -2)
            {
                return -2;
            }
        }

        if ($this->input[$this->cursor] == "=")
        {
            $this->buffer .= "=";
            $this->cursor++;

            if ($this->cursor >= $this->max)
            {
                $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS);
                $this->buffer = "";
                return -2;
            }
        }
        else
        {
            // This also accepts invalid attributes (only names without value).

            if(strcasecmp($attributeName, "href") === 0 ||
               $this->input[$this->cursor] == "<" ||
               $this->input[$this->cursor] == "/")
            {
                $this->buffer .= $this->input[$this->cursor];

                $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_NOASSIGNMENTAFTERATTRIBUTENAME);
                $this->buffer = "";
                return -1;
            }
            else if ($this->input[$this->cursor] == ">")
            {
                return -1;
            }
            else
            {
                return null;
            }
        }

        if (ctype_space($this->input[$this->cursor]) === true)
        {
            if ($this->consumeWhitespace(true) == -2)
            {
                return -2;
            }
        }

        $delimiter = $this->input[$this->cursor];
        $this->buffer .= $delimiter;

        if ($delimiter != "'" &&
            $delimiter != "\"")
        {
            $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_NOATTRIBUTEVALUEDELIMITER);
            $this->buffer = "";
            return -1;
        }

        $attributeValue = "";

        do
        {
            $this->cursor++;

            if ($this->cursor >= $this->max)
            {
                $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS);
                $this->buffer = "";
                return -2;
            }

            if ($this->input[$this->cursor] == $delimiter)
            {
                $this->buffer .= $delimiter;
                $this->cursor++;

                if ($this->cursor >= $this->max)
                {
                    $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS);
                    $this->buffer = "";
                    return -2;
                }

                break;
            }

            $attributeValue .= $this->input[$this->cursor];
            $this->buffer .= $this->input[$this->cursor];

        } while (true);

        if (strcasecmp($attributeName, "href") === 0 &&
            strlen($attributeValue) > 0)
        {
            $this->links[] = $attributeValue;
            return $attributeValue;
        }

        return null;
    }

    protected function consumeWhitespace($record)
    {
        while (ctype_space($this->input[$this->cursor]) === true)
        {
            if ($record == true)
            {
                $this->buffer .= $this->input[$this->cursor];
            }

            $this->cursor++;

            if ($this->cursor >= $this->max)
            {
                $this->errors[] = array("buffer" => $this->buffer, "code" => CUSTOMHTMLREADER_ERRORCODE_OUTOFCHARACTERS);
                $this->buffer = "";
                return -2;
            }
        }

        return 0;
    }

    public function getLinks()
    {
        return $this->links;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected $cursor = 0;
    protected $max = 0;
    protected $input;
    protected $links = array();
    protected $buffer = "";
    protected $errors = array();
}

?>

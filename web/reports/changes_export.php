<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/reports/changes_export.php
 * @author Stephan Kreutzer
 * @since 2023-05-08
 */


require_once("../libraries/https.inc.php");
require_once("../libraries/session.inc.php");


require_once("../libraries/bucket_management.inc.php");


if (isset($_GET["bucket-id"]) !== true)
{
    http_response_code(400);
    echo "'bucket-id' is missing.";
    exit(1);
}

$bucketId = (int)$_GET["bucket-id"];

if (checkBucketPermission($bucketId) !== true)
{
    http_response_code(403);
    exit(0);
}


require_once("../libraries/csv_utilities.inc.php");
require_once("../libraries/change_management.inc.php");

$changes = getChanges($bucketId);

if (is_array($changes) === true)
{
    $changesByUrl = array();

    for ($i = 0, $max = count($changes); $i < $max; $i++)
    {
        $sourceUrl = $changes[$i]["resource_source_url"];

        if (isset($changesByUrl[$sourceUrl]) != true)
        {
            $changesByUrl[$sourceUrl] = array();
        }

        $changesByUrl[$sourceUrl][] = $changes[$i];
    }

    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=\"".((int)$bucketId)."_changes.csv\"");

    echo "\"url-source\",\"action\",\"url-target\"\r\n";

    foreach ($changesByUrl as $url => $changes)
    {
        for ($i = 0, $max = count($changes); $i < $max; $i++)
        {
            echo "\"".escapeCsvString($url)."\",\"";

            if (((int)$changes[$i]["link_change_action"]) == CHANGE_ACTION_REMOVE)
            {
                echo "remove";
            }
            else if (((int)$changes[$i]["link_change_action"]) == CHANGE_ACTION_ADD)
            {
                echo "add";
            }
            else
            {
                echo "?";
            }

            echo "\",\"".escapeCsvString($changes[$i]["resource_target_url"])."\"\r\n";
        }
    }
}


?>

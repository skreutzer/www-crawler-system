<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/reports/changes.php
 * @author Stephan Kreutzer
 * @since 2023-04-30
 */


require_once("../libraries/https.inc.php");
require_once("../libraries/session.inc.php");


require_once("../libraries/bucket_management.inc.php");


if (isset($_GET["bucket-id"]) !== true)
{
    http_response_code(400);
    echo "'bucket-id' is missing.";
    exit(1);
}

$bucketId = (int)$_GET["bucket-id"];

if (checkBucketPermission($bucketId) !== true)
{
    http_response_code(403);
    exit(0);
}

require_once("../libraries/change_management.inc.php");


require_once("../libraries/languagelib.inc.php");
require_once(getLanguageFile("changes"));


echo "<!DOCTYPE html>\n".
     "<html xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"../mainstyle.css\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n";

$changes = getChanges($bucketId);

if (is_array($changes) === true)
{
    $changesByUrl = array();

    for ($i = 0, $max = count($changes); $i < $max; $i++)
    {
        $sourceUrl = $changes[$i]["resource_source_url"];

        if (isset($changesByUrl[$sourceUrl]) != true)
        {
            $changesByUrl[$sourceUrl] = array();
        }

        $changesByUrl[$sourceUrl][] = $changes[$i];
    }

    echo "        <div>\n";

    foreach ($changesByUrl as $url => $changes)
    {
        echo "          <div>\n".
             "            <div><a href=\"".htmlspecialchars($url, ENT_HTML5 | ENT_QUOTES, "UTF-8")."\">".htmlspecialchars($url, ENT_HTML5, "UTF-8")."</a></div>\n".
             "            <ul>\n";

        for ($i = 0, $max = count($changes); $i < $max; $i++)
        {
            echo "              <li>\n".
                 "                <span>";

            if (((int)$changes[$i]["link_change_action"]) == CHANGE_ACTION_REMOVE)
            {
                echo LANG_TEXTCAPTION_REMOVE;
            }
            else if (((int)$changes[$i]["link_change_action"]) == CHANGE_ACTION_ADD)
            {
                echo LANG_TEXTCAPTION_ADD;
            }
            else
            {
                echo "?";
            }

            echo "</span>".
                 "<a href=\"".htmlspecialchars($changes[$i]["resource_target_url"], ENT_HTML5 | ENT_QUOTES, "UTF-8")."\">".htmlspecialchars($changes[$i]["resource_target_url"], ENT_HTML5, "UTF-8")."</a>\n";

            echo "              </li>\n";
        }

        echo "            </ul>\n".
             "          </div>\n";
    }

    echo "        </div>\n";
}

echo "        <div>\n".
     "          <a href=\"../view_crawls.php\">".LANG_LINKCAPTION_PROJECTS."</a><br/>\n".
     "          <a href=\"../index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
     "        </div>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>

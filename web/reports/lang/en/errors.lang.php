<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/reports/lang/en/errors.lang.php
 * @author Stephan Kreutzer
 * @since 2023-05-17
 */


define("LANG_PAGETITLE", "HTTP Status Codes");
define("LANG_HEADER", "HTTP Status Codes");
define("LANG_LINKCAPTION_PROJECTS", "Projects page");
define("LANG_LINKCAPTION_MAINPAGE", "Main page");


?>

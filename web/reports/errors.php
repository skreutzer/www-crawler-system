<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/reports/changes.php
 * @author Stephan Kreutzer
 * @since 2023-05-17
 */


require_once("../libraries/https.inc.php");
require_once("../libraries/session.inc.php");


require_once("../libraries/bucket_management.inc.php");


if (isset($_GET["bucket-id"]) !== true)
{
    http_response_code(400);
    echo "'bucket-id' is missing.";
    exit(1);
}

$bucketId = (int)$_GET["bucket-id"];

if (checkBucketPermission($bucketId) !== true)
{
    http_response_code(403);
    exit(0);
}

require_once("../libraries/resource_management.inc.php");


require_once("../libraries/languagelib.inc.php");
require_once(getLanguageFile("errors"));


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"../mainstyle.css\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n";

$errors = getResourcesByHttpStatusCode($bucketId);

if (is_array($errors) === true)
{
    $errorsByUrl = array();

    for ($i = 0, $max = count($errors); $i < $max; $i++)
    {
        $sourceUrl = $errors[$i]["resource_source_url"];

        if (isset($errorsByUrl[$sourceUrl]) != true)
        {
            $errorsByUrl[$sourceUrl] = array();
        }

        $errorsByUrl[$sourceUrl][] = $errors[$i];
    }

    echo "        <ul>\n";

    foreach ($errorsByUrl as $url => $errors)
    {
        echo "          <li>\n".
             "            <a href=\"".htmlspecialchars($url, ENT_XHTML | ENT_QUOTES, "UTF-8")."\">".htmlspecialchars($url, ENT_XHTML, "UTF-8")."</a>:\n".
             "            <ul>\n";

        for ($i = 0, $max = count($errors); $i < $max; $i++)
        {
            echo "              <li>\n".
                 "                <a href=\"".htmlspecialchars($errors[$i]["resource_url"], ENT_XHTML | ENT_QUOTES, "UTF-8")."\">".htmlspecialchars($errors[$i]["resource_url"], ENT_XHTML, "UTF-8")."</a>: ".((int)$errors[$i]["resource_http_response_code"])."\n".
                 "              </li>\n";
        }

        echo "            </ul>\n".
             "          </li>\n";
    }

    echo "        </ul>\n";
}

echo "        <div>\n".
     "          <a href=\"../view_crawls.php\">".LANG_LINKCAPTION_PROJECTS."</a><br/>\n".
     "          <a href=\"../index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
     "        </div>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>

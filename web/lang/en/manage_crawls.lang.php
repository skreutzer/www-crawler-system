<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/manage_crawls.lang.php
 * @author Stephan Kreutzer
 * @since 2023-03-20
 */


define("LANG_PAGETITLE", "Manage Crawls");
define("LANG_HEADER", "Manage Crawls");
define("LANG_TABLEHEADER_BUCKETURL", "Start URL");
define("LANG_TABLEHEADER_ACTIONS", "Actions");
define("LANG_FORMLABEL_URL", "Start URL");
define("LANG_FORMLABEL_ADD", "Add");
define("LANG_LINKCAPTION_CRAWLCONFIGURE", "configure");
define("LANG_LINKCAPTION_CRAWLRUN", "run");
define("LANG_LINKCAPTION_CRAWLRERUN", "re-run");
define("LANG_LINKCAPTION_MAINPAGE", "Main page");



?>

<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/configure_crawl.lang.php
 * @author Stephan Kreutzer
 * @since 2023-03-20
 */


define("LANG_PAGETITLE", "Configure Crawl");
define("LANG_HEADER", "Configure Crawl");
define("LANG_HEADER_CRAWLFORSTARTURL", "Crawl for start URL ");
define("LANG_TABLEHEADER_USERNAME", "User");
define("LANG_TABLEHEADER_SELECT", "Select");
define("LANG_FORMLABEL_UPDATE", "Update");
define("LANG_TABLEHEADER_URL", "URL");
define("LANG_TABLEHEADER_THRESHOLD", "Threshold");
define("LANG_FORMLABEL_URL", "URL");
define("LANG_FORMLABEL_THRESHOLD", "Threshold");
define("LANG_BUTTONCAPTION_ADD", "Add");
define("LANG_LINKCAPTION_CRAWLSPAGE", "Crawls page");
define("LANG_LINKCAPTION_MAINPAGE", "Main page");



?>

<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/view_crawls.lang.php
 * @author Stephan Kreutzer
 * @since 2023-03-20
 */


define("LANG_PAGETITLE", "My Projects");
define("LANG_HEADER", "My Projects");
define("LANG_LINKCAPTION_GRAPHRADIALTREE", "radial tree");
define("LANG_LINKCAPTION_GRAPHGRID", "grid");
define("LANG_LINKCAPTION_CHANGEREPORT", "change report");
define("LANG_LINKCAPTION_HTTPERRORCODES", "HTTP error codes");
define("LANG_TEXT_CSVEXPORT", "CSV export");
define("LANG_LINKCAPTION_CSVEXPORT_NODES", "nodes");
define("LANG_LINKCAPTION_CSVEXPORT_EDGES", "edges");
define("LANG_LINKCAPTION_CSVEXPORT_CHANGEREPORT", "change report");
define("LANG_LINKCAPTION_CSVEXPORT_HTTPERRORCODES", "HTTP error codes");
define("LANG_TEXT_STATS", "%d URLs, %d links");
define("LANG_LINKCAPTION_MAINPAGE", "Main page");



?>

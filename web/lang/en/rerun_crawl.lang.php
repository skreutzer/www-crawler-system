<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/lang/en/rerun_crawl.lang.php
 * @author Stephan Kreutzer
 * @since 2023-05-12
 */


define("LANG_PAGETITLE", "Re-run Crawl");
define("LANG_HEADER", "Re-run Crawl");
define("LANG_TEXT_URLSPREPARED", "The following URLs have been prepared to be re-crawled:");
define("LANG_TEXT_NOURLS", "No URLs to prepare for being re-crawled, as there’s no link change recommendations set.");
define("LANG_LINKCAPTION_CRAWLRUN", "run");
define("LANG_LINKCAPTION_MAINPAGE", "Main page");



?>

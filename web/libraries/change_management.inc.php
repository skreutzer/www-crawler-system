<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/change_management.inc.php
 * @author Stephan Kreutzer
 * @since 2023-04-30
 */


require_once(dirname(__FILE__)."/database.inc.php");


define("CHANGE_ACTION_UNKNOWN", 0);
define("CHANGE_ACTION_GET", 1);
define("CHANGE_ACTION_REMOVE", 2);
define("CHANGE_ACTION_REMOVEUNDO", 3);
define("CHANGE_ACTION_ADD", 4);
define("CHANGE_ACTION_ADDUNDO", 5);


function changeGetBySourceTargetId($bucketId, $idSource, $idTarget, $action)
{
    $change = Database::Get()->query("SELECT `id`,\n".
                                     "    `action`,\n".
                                     "    `url`\n".
                                     "FROM `".Database::GetPrefix()."link_change`\n".
                                     "WHERE `id_bucket`=".((int)$bucketId)." AND\n".
                                     "    `id_resource_source`=".((int)$idSource)." AND\n".
                                     "    `id_resource_target`=".((int)$idTarget)." AND\n".
                                     "    `action`=".((int)$action)."\n");

    if ($change === false)
    {
        return -1;
    }

    $change = Database::GetResultAssoc($change);
    $changeCount = count($change);

    if ($changeCount > 1)
    {
        // Wait, what? Duplicates?
        return -2;
    }

    return $change;
}

function changeRemove($bucketId, $idSource, $idTarget)
{
    $change = changeGetBySourceTargetId($bucketId, $idSource, $idTarget, CHANGE_ACTION_REMOVE);

    if (is_array($change) !== true)
    {
        return -1;
    }

    if (count($change) > 0)
    {
        if ((int)($change[0]["action"]) == CHANGE_ACTION_REMOVE)
        {
            return -2;
        }
        else
        {
            return -3;
        }
    }

    $result = Database::Get()->query("INSERT INTO `".Database::GetPrefix()."link_change` (`id`,\n".
                                     "    `action`,\n".
                                     "    `id_resource_source`,\n".
                                     "    `id_resource_target`,\n".
                                     "    `url`,\n".
                                     "    `id_bucket`)\n".
                                     "VALUES (NULL,\n".
                                     "    ".CHANGE_ACTION_REMOVE.",\n".
                                     "    ".($idSource).",\n".
                                     "    ".($idTarget).",\n".
                                     "    NULL,\n".
                                     "    ".(int)$bucketId.")");

    if ($result !== true)
    {
        return -4;
    }

    if (Database::Get()->insert_id === NULL)
    {
        return -5;
    }

    return Database::Get()->insert_id;
}

function changeRemoveUndo($bucketId, $idSource, $idTarget)
{
    $result = Database::Get()->query("DELETE FROM `".Database::GetPrefix()."link_change`\n".
                                     "WHERE `id_resource_source`=".((int)$idSource)." AND\n".
                                     "    `id_resource_target`=".((int)$idTarget)." AND\n".
                                     "    `action`=".CHANGE_ACTION_REMOVE." AND\n".
                                     "    `id_bucket`=".((int)$bucketId));

    /** @todo Check if the record was actually deleted,
      * in order to create a different result/response. */

    if ($result !== true)
    {
        return -1;
    }

    return 0;
}

/**
 * @todo Check if already existing, if ID to ID (internal URL)
 *     or ID to URL (external URL).
 */
function changeAdd($bucketId, $idSource, $idTarget, $url, &$urlToIdResolve)
{
    if ($idSource === null &&
        $idTarget === null)
    {
        return -1;
    }

    {
        // This is just to check if a resource with this ID
        // exists and is also in the correct bucket.

        require_once(dirname(__FILE__)."/resource_management.inc.php");

        $id = ($idSource !== null ? $idSource : $idTarget);

        $resource = Database::Get()->query("SELECT `id`,\n".
                                           "    `url`\n".
                                           "FROM `".Database::GetPrefix()."resource`\n".
                                           "WHERE `id`=".((int)$id)." AND\n".
                                           "    `id_bucket`=".((int)$bucketId));

        if ($resource === false)
        {
            return -2;
        }

        $resource = Database::GetResultAssoc($resource);
        $resourceCount = count($resource);

        if ($resourceCount < 1)
        {
            return -3;
        }

        if ($resourceCount > 1)
        {
            // Wait, what? Duplicates?
            return -4;
        }
    }

    $resourceUrl = Database::Get()->query("SELECT `id`\n".
                                          "FROM `".Database::GetPrefix()."resource`\n".
                                          "WHERE `url` LIKE \"".Database::Get()->real_escape_string($url)."\" AND\n".
                                          "    `id_bucket`=".((int)$bucketId));

    if ($resourceUrl === false)
    {
        return -5;
    }

    $resourceUrl = Database::GetResultAssoc($resourceUrl);
    $resourceUrlCount = count($resourceUrl);

    if ($resourceUrlCount > 1)
    {
        // Wait, what? Duplicates?
        return -6;
    }

    if ($resourceUrlCount <= 0)
    {
        if ($idSource == null)
        {
            /** @retval -13 No point in recommending to add an incoming link
              * from another external page, as no control over external sites. */
            return -13;
        }
    }

    if ($resourceUrlCount == 1)
    {
        $resourceUrl = $resourceUrl[0];
        $urlToIdResolve = (int)$resourceUrl["id"];

        if ($idSource === null)
        {
            $idSource = (int)$resourceUrl["id"];
        }
        else
        {
            $idTarget = (int)$resourceUrl["id"];
        }
    }

    if ($idSource !== null &&
        $idTarget !== null)
    {
        $result = Database::Get()->query("INSERT INTO `".Database::GetPrefix()."link_change` (`id`,\n".
                                         "    `action`,\n".
                                         "    `id_resource_source`,\n".
                                         "    `id_resource_target`,\n".
                                         "    `url`,\n".
                                         "    `id_bucket`)\n".
                                         "VALUES (NULL,\n".
                                         "    ".CHANGE_ACTION_ADD.",\n".
                                         "    ".((int)$idSource).",\n".
                                         "    ".((int)$idTarget).",\n".
                                         "    NULL,\n".
                                         "    ".(int)$bucketId.")");

        if ($result !== true)
        {
            return -7;
        }

        if (Database::Get()->insert_id === NULL)
        {
            return -8;
        }

        return Database::Get()->insert_id;
    }
    else
    {
        if ($idSource !== null)
        {
            $result = Database::Get()->query("INSERT INTO `".Database::GetPrefix()."link_change` (`id`,\n".
                                             "    `action`,\n".
                                             "    `id_resource_source`,\n".
                                             "    `id_resource_target`,\n".
                                             "    `url`,\n".
                                             "    `id_bucket`)\n".
                                             "VALUES (NULL,\n".
                                             "    ".CHANGE_ACTION_ADD.",\n".
                                             "    ".((int)$idSource).",\n".
                                             "    NULL,\n".
                                             "    \"".Database::Get()->real_escape_string($url)."\",\n".
                                             "    ".(int)$bucketId.")");

            if ($result !== true)
            {
                return -9;
            }

            if (Database::Get()->insert_id === NULL)
            {
                return -10;
            }

            return Database::Get()->insert_id;
        }
        else
        {
            $result = Database::Get()->query("INSERT INTO `".Database::GetPrefix()."link_change` (`id`,\n".
                                             "    `action`,\n".
                                             "    `id_resource_source`,\n".
                                             "    `id_resource_target`,\n".
                                             "    `url`,\n".
                                             "    `id_bucket`)\n".
                                             "VALUES (NULL,\n".
                                             "    ".CHANGE_ACTION_ADD.",\n".
                                             "    NULL,\n".
                                             "    ".((int)$idTarget).",\n".
                                             "    \"".Database::Get()->real_escape_string($url)."\",\n".
                                             "    ".(int)$bucketId.")");

            if ($result !== true)
            {
                return -11;
            }

            if (Database::Get()->insert_id === NULL)
            {
                return -12;
            }

            return Database::Get()->insert_id;
        }
    }

    return -14;
}

function changeAddUndo($bucketId, $idSource, $idTarget, $url)
{
    if ($idSource == null &&
        $idTarget == null)
    {
        return -1;
    }

    if ($idSource == null ||
        $idTarget == null)
    {
        if ($url == null)
        {
            return -2;
        }
    }

    $sql = "";

    if ($idSource != null &&
        $idTarget != null)
    {
        $sql = "`id_resource_source`=".((int)$idSource)." AND\n".
               "`id_resource_target`=".((int)$idTarget);
    }
    else if ($idSource != null &&
             $idTarget == null)
    {
        $sql = "`id_resource_source`=".((int)$idSource)." AND\n".
               "`url` LIKE \"".Database::Get()->real_escape_string($url)."\"";
    }
    else if ($idSource == null &&
             $idTarget != null)
    {
        $sql = "`id_resource_target`=".((int)$idTarget)." AND\n".
               "`url` LIKE \"".Database::Get()->real_escape_string($url)."\"";
    }

    $result = Database::Get()->query("DELETE FROM `".Database::GetPrefix()."link_change`\n".
                                     "WHERE ".$sql." AND\n".
                                     "    `action`=".CHANGE_ACTION_ADD." AND\n".
                                     "    `id_bucket`=".((int)$bucketId));

    if ($result !== true)
    {
        return -3;
    }

    return 0;
}

function getChanges($bucketId)
{
    // CTE: Common Table Expression.
    $changes = Database::Get()->query("WITH `cte` AS\n".
                                      "(\n".
                                      "    SELECT `".Database::GetPrefix()."link_change`.`id` AS `link_change_id`,\n".
                                      "        `".Database::GetPrefix()."link_change`.`action` AS `link_change_action`,\n".
                                      "        `".Database::GetPrefix()."link_change`.`id_resource_source` AS `link_change_id_resource_source`,\n".
                                      "        `".Database::GetPrefix()."link_change`.`id_resource_target` AS `link_change_id_resource_target`,\n".
                                      "        `".Database::GetPrefix()."link_change`.`url` AS `link_change_url`,\n".
                                      "        `".Database::GetPrefix()."resource`.`url` AS `resource_source_url`\n".
                                      "    FROM `".Database::GetPrefix()."link_change`\n".
                                      "    LEFT JOIN `".Database::GetPrefix()."resource` ON\n".
                                      "        `".Database::GetPrefix()."resource`.`id` =\n".
                                      "        `".Database::GetPrefix()."link_change`.`id_resource_source`\n".
                                      "    WHERE `".Database::GetPrefix()."link_change`.`id_bucket`=".((int)$bucketId)."\n".
                                      ")\n".
                                      "SELECT `cte`.*,\n".
                                      "    `".Database::GetPrefix()."resource`.`url` AS `resource_target_url`\n".
                                      "FROM `cte`\n".
                                      "LEFT JOIN `".Database::GetPrefix()."resource` ON\n".
                                      "    `cte`.`link_change_id_resource_target` =\n".
                                      "    `".Database::GetPrefix()."resource`.`id`\n");

    if ($changes === false)
    {
        return -1;
    }

    $changes = Database::GetResultAssoc($changes);

    for ($i = 0, $max = count($changes); $i < $max; $i++)
    {
        if ($changes[$i]["link_change_id_resource_source"] == null)
        {
            $changes[$i]["resource_source_url"] = $changes[$i]["link_change_url"];
        }

        if ($changes[$i]["link_change_id_resource_target"] == null)
        {
            $changes[$i]["resource_target_url"] = $changes[$i]["link_change_url"];
        }

        unset($changes[$i]["link_change_url"]);
    }

    return $changes;

    /*
    // All of this is bad and ugly :-( Not needed, replaced by CTE above.

    $changes = Database::Get()->query("SELECT `".Database::GetPrefix()."link_change`.`id` AS `link_change_id`,\n".
                                      "    `".Database::GetPrefix()."link_change`.`action` AS `link_change_action`,\n".
                                      "    `".Database::GetPrefix()."link_change`.`id_resource_source` AS `link_change_id_resource_source`,\n".
                                      "    `".Database::GetPrefix()."link_change`.`id_resource_target` AS `link_change_id_resource_target`,\n".
                                      "    `".Database::GetPrefix()."link_change`.`url` AS `link_change_url`,\n".
                                      "    `".Database::GetPrefix()."resource`.`url` AS `resource_url`\n".
                                      "FROM `".Database::GetPrefix()."resource`\n".
                                      "LEFT JOIN `".Database::GetPrefix()."link_change` ON\n".
                                      "    `".Database::GetPrefix()."link_change`.`id_resource_source` =\n".
                                      "    `".Database::GetPrefix()."resource`.`id` OR\n".
                                      "    `".Database::GetPrefix()."link_change`.`id_resource_target` =\n".
                                      "    `".Database::GetPrefix()."resource`.`id`\n".
                                      "WHERE `".Database::GetPrefix()."link_change`.`id_bucket`=".((int)$bucketId));

    if ($changes === false)
    {
        return -1;
    }

    $changes = Database::GetResultAssoc($changes);

    $result = array();

    for ($i = 0, $max = count($changes); $i < $max; $i++)
    {
        $linkId = (int)$changes[$i]["link_change_id"];

        if ($changes[$i]["link_change_id_resource_source"] != null &&
            $changes[$i]["link_change_id_resource_target"] != null)
        {
            if (isset($result[$linkId]) != true)
            {
                $result[$linkId] = $changes[$i];
                $result[$linkId]["resource_url"] = array($changes[$i]["resource_url"]);
            }
            else
            {
                $result[$linkId]["resource_url"][] = $changes[$i]["resource_url"];
            }
        }
        else
        {
            if (isset($result[$linkId]) == true)
            {
                // Huh, how, why?
                return -2;
            }

            $result[$linkId] = $changes[$i];

            if ($changes[$i]["link_change_id_resource_source"] != null)
            {
                $result[$linkId]["resource_source_url"] = $changes[$i]["resource_url"];
            }
            else
            {
                $result[$linkId]["resource_source_url"] = $changes[$i]["link_change_url"];
            }

            if ($changes[$i]["link_change_id_resource_target"] != null)
            {
                $result[$linkId]["resource_target_url"] = $changes[$i]["resource_url"];
            }
            else
            {
                $result[$linkId]["resource_target_url"] = $changes[$i]["link_change_url"];
            }

            unset($result[$linkId]["resource_url"]);
        }

        unset($result[$linkId]["link_change_url"]);
    }

    $targetUrls = array();

    {
        $targets = Database::Get()->query("SELECT `".Database::GetPrefix()."link_change`.`id` AS `link_change_id`,\n".
                                          "    `".Database::GetPrefix()."link_change`.`id_resource_target` AS `link_change_id_resource_target`,\n".
                                          "    `".Database::GetPrefix()."resource`.`url` AS `resource_url`\n".
                                          "FROM `".Database::GetPrefix()."resource`\n".
                                          "LEFT JOIN `".Database::GetPrefix()."link_change` ON\n".
                                          "    `".Database::GetPrefix()."link_change`.`id_resource_target` =\n".
                                          "    `".Database::GetPrefix()."resource`.`id`\n".
                                          "WHERE `".Database::GetPrefix()."link_change`.`id_bucket`=".((int)$bucketId)." AND\n".
                                          "    (`".Database::GetPrefix()."link_change`.`id_resource_source` IS NOT NULL AND\n".
                                          "     `".Database::GetPrefix()."link_change`.`id_resource_target` IS NOT NULL)");

        if ($targets === false)
        {
            return -2;
        }

        $targets = Database::GetResultAssoc($targets);

        for ($i = 0, $max = count($targets); $i < $max; $i++)
        {
            //$targetUrls[(int)$targets[$i]["link_change_id_resource_target"]] = $targets[$i]["resource_url"];
            $targetUrls[(int)$targets[$i]["link_change_id"]] = $targets[$i]["resource_url"];
        }
    }

    foreach ($result as $linkId => &$values)
    {
        if ($values["link_change_id_resource_source"] == null ||
            $values["link_change_id_resource_target"] == null)
        {
            continue;
        }

        if (isset($targetUrls[$linkId]) != true)
        {
            return -3;
        }


        if (count($values["resource_url"]) != 2)
        {
            return -4;
        }

        if ($targetUrls[$linkId] == $values["resource_url"][0])
        {
            $values["resource_source_url"] = $values["resource_url"][1];
            $values["resource_target_url"] = $values["resource_url"][0];
        }
        else if ($targetUrls[$linkId] == $values["resource_url"][1])
        {
            $values["resource_source_url"] = $values["resource_url"][0];
            $values["resource_target_url"] = $values["resource_url"][1];
        }
        else
        {
            return -5;
        }

        unset($values["resource_url"]);
    }

    return array_values($result);
    */
}

function getChangesByResourceId($bucketId, $resourceId)
{
    $changes = Database::Get()->query("SELECT `id`,\n".
                                      "    `action`,\n".
                                      "    `id_resource_source`,\n".
                                      "    `id_resource_target`,\n".
                                      "    `url`\n".
                                      "FROM `".Database::GetPrefix()."link_change`\n".
                                      "WHERE `id_bucket`=".((int)$bucketId)." AND\n".
                                      "    (`id_resource_source`=".((int)$resourceId)." OR\n".
                                      "     `id_resource_target`=".((int)$resourceId).")\n");

    if ($changes === false)
    {
        return -1;
    }

    $changes = Database::GetResultAssoc($changes);

    return $changes;
}

function clearChanges($bucketId)
{
    $result = Database::Get()->query("DELETE FROM `".Database::GetPrefix()."link_change`\n".
                                     "WHERE `id_bucket`=".((int)$bucketId));

    /** @todo Check if the record was actually deleted,
      * in order to create a different result/response. */

    if ($result !== true)
    {
        return -1;
    }

    return 0;
}


?>

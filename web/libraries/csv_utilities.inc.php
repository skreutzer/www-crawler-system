<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/csv_utilities.inc.php
 * @author Stephan Kreutzer
 * @since 2023-05-08
 */


function escapeCsvString($input)
{
    $result = "";

    for ($i = 0, $max = strlen($input); $i < $max; $i++)
    {
        if ($input[$i] == "\"")
        {
            $result .= "\"\"";
        }
        else
        {
            $result .= $input[$i];
        }
    }

    return $result;
}


?>

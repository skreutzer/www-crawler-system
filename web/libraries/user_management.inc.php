<?php
/* Copyright (C) 2012-2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/libraries/user_management.inc.php
 * @author Stephan Kreutzer
 * @since 2012-06-02
 */


require_once(dirname(__FILE__)."/database.inc.php");
require_once(dirname(__FILE__)."/user_defines.inc.php");


function insertNewUser($name, $password, $role)
{
    /** @todo Check for empty $name, $password or $role. */

    $salt = md5(uniqid(rand(), true));
    $password = hash('sha512', $salt.$password);

    if (Database::Get()->query("INSERT INTO `".Database::GetPrefix()."user` (`id`,\n".
                               "    `name`,\n".
                               "    `salt`,\n".
                               "    `password`,\n".
                               "    `role`)\n".
                               "VALUES (NULL,\n".
                               "    \"".Database::Get()->real_escape_string($name)."\",\n".
                               "    \"".Database::Get()->real_escape_string($salt)."\",\n".
                               "    \"".Database::Get()->real_escape_string($password)."\",\n".
                               "    ".((int)$role).")") !== true)
    {
        return -1;
    }

    $id = Database::Get()->insert_id;

    if ($id <= 0)
    {
        return -2;
    }

    return $id;
}

function getUserByName($name)
{
    /** @todo Check for empty $name. */

    $user = Database::Get()->query("SELECT `id`,\n".
                                   "    `salt`,\n".
                                   "    `password`,\n".
                                   "    `role`\n".
                                   "FROM `".Database::GetPrefix()."user`\n".
                                   "WHERE `name` LIKE \"".Database::Get()->real_escape_string($name)."\"");

    if ($user == false)
    {
        return -1;
    }

    $user = Database::GetResultAssoc($user);

    return $user;
}

function getUsers()
{
    $users = Database::Get()->query("SELECT `id`,\n".
                                    "    `name`,\n".
                                    "    `role`\n".
                                    "FROM `".Database::GetPrefix()."user`\n".
                                    "WHERE 1\n");

    if ($users == false)
    {
        return -1;
    }

    $users = Database::GetResultAssoc($users);

    if (is_array($users) !== true)
    {
        return -2;
    }

    return $users;
}



?>

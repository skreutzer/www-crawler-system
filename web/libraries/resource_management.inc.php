<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/resource_management.inc.php
 * @author Stephan Kreutzer
 * @since 2023-01-24
 */


require_once(dirname(__FILE__)."/database.inc.php");
require_once(dirname(__FILE__)."/resource_defines.inc.php");


/**
 * @param[in] $byId If true, $selector needs to be a node ID. If false,
 *     $selector needs to be an URL.
 */
function getResource($bucketId, $byId, $selector, $includeLinks)
{
    $sql = "SELECT `id`,\n".
           "    `url`,\n".
           "    `last_retrieval`\n".
           "FROM `".Database::GetPrefix()."resource`\n";

    if ($byId === true)
    {
        $sql .= "WHERE `id`=".((int)$selector);
    }
    else
    {
        $sql .= "WHERE `url` LIKE \"".Database::Get()->real_escape_string($selector)."\"";
    }

    $sql .= " AND\n".
            "    `id_bucket`=".((int)$bucketId);

    $resource = Database::Get()->query($sql);

    if ($resource === false)
    {
        return -1;
    }

    $resource = Database::GetResultAssoc($resource);
    $resourceCount = count($resource);

    if ($resourceCount > 1)
    {
        // Wait, what? Duplicates?
        return -2;
    }

    if ($includeLinks === true &&
        $resourceCount > 0)
    {
        /** @todo Review the JOIN. */
        $links = Database::Get()->query("SELECT `".Database::GetPrefix()."link`.`id` AS `link_id`,\n".
                                        "    `".Database::GetPrefix()."link`.`id_resource_source` AS `link_id_resource_source`,\n".
                                        "    `".Database::GetPrefix()."link`.`id_resource_target` AS `link_id_resource_target`,\n".
                                        "    `".Database::GetPrefix()."link`.`contexts` AS `link_contexts`,\n".
                                        //"    `".Database::GetPrefix()."resource`.`id` AS `resource_id`,\n".
                                        "    `".Database::GetPrefix()."resource`.`url` AS `resource_url`\n".
                                        "FROM `".Database::GetPrefix()."resource`\n".
                                        "INNER JOIN `".Database::GetPrefix()."link` ON\n".
                                        "    `".Database::GetPrefix()."link`.`id_resource_source` =\n".
                                        "    `".Database::GetPrefix()."resource`.`id` OR\n".
                                        "    `".Database::GetPrefix()."link`.`id_resource_target` =\n".
                                        "    `".Database::GetPrefix()."resource`.`id`\n".
                                        "WHERE NOT `".Database::GetPrefix()."resource`.`id`=".((int)$resource[0]["id"])." AND\n".
                                        "    (`".Database::GetPrefix()."link`.`id_resource_source`=".((int)$resource[0]["id"])." OR\n".
                                        "     `".Database::GetPrefix()."link`.`id_resource_target`=".((int)$resource[0]["id"]).")");

        if ($links === false)
        {
            return -1;
        }

        $links = Database::GetResultAssoc($links);

        $resource[0]["links"]["incoming"] = array();
        $resource[0]["links"]["outgoing"] = array();

        for ($i = 0, $max = count($links); $i < $max; $i++)
        {
            $link["id"] = (int)$links[$i]["link_id"];
            $link["url"] = $links[$i]["resource_url"];
            $link["contexts"] = (int)$links[$i]["link_contexts"];

            if (((int)$links[$i]["link_id_resource_source"]) != ((int)$resource[0]["id"]))
            {
                $link["id-node"] = (int)$links[$i]["link_id_resource_source"];
                $resource[0]["links"]["incoming"][] = $link;
            }
            else
            {
                $link["id-node"] = (int)$links[$i]["link_id_resource_target"];
                $resource[0]["links"]["outgoing"][] = $link;
            }
        }
    }

    return $resource;
}

/** @todo Does this still work properly without CTE, or is it just outdated/flawed? */
function getResourcesPaginated($bucketId, $offset, $limit)
{
    $resources = Database::Get()->query("SELECT `id`,\n".
                                        "    `url`\n".
                                        "FROM `".Database::GetPrefix()."resource`\n".
                                        "WHERE `id_bucket`=".((int)$bucketId)."\n".
                                        "ORDER BY `id`\n"/*.
                                        "LIMIT ".((int)$limit)." OFFSET ".((int)$offset)*/);

    if ($resources === false)
    {
        return -1;
    }

    return Database::GetResultAssoc($resources);
}

function getResourcesByHttpStatusCode($bucketId)
{
    $resources = Database::Get()->query("WITH `cte` AS\n".
                                        "(\n".
                                        "    SELECT `".Database::GetPrefix()."link`.`id_resource_source` AS `link_id_resource_source`,\n".
                                        "        `".Database::GetPrefix()."resource`.`id` AS `resource_id`,\n".
                                        "        `".Database::GetPrefix()."resource`.`url` AS `resource_url`,\n".
                                        "        `".Database::GetPrefix()."resource`.`http_response_code` AS `resource_http_response_code`\n".
                                        "    FROM `".Database::GetPrefix()."link`\n".
                                        "    LEFT JOIN `".Database::GetPrefix()."resource` ON\n".
                                        "        `".Database::GetPrefix()."resource`.`id` =\n".
                                        "        `".Database::GetPrefix()."link`.`id_resource_target`\n".
                                        "    WHERE `".Database::GetPrefix()."resource`.`id_bucket`=".((int)$bucketId)." AND\n".
                                        "        `".Database::GetPrefix()."resource`.`http_response_code` IS NOT NULL AND\n".
                                        "        `".Database::GetPrefix()."resource`.`http_response_code` != 200\n".
                                        ")\n".
                                        "SELECT `cte`.*,\n".
                                        "    `".Database::GetPrefix()."resource`.`url` AS `resource_source_url`\n".
                                        "FROM `cte`\n".
                                        "LEFT JOIN `".Database::GetPrefix()."resource` ON\n".
                                        "    `cte`.`link_id_resource_source` =\n".
                                        "    `".Database::GetPrefix()."resource`.`id`\n");

    if ($resources === false)
    {
        return -1;
    }

    return Database::GetResultAssoc($resources);

    /*
    $resources = Database::Get()->query("SELECT `id`,\n".
                                        "    `url`,\n".
                                        "    `http_response_code`\n".
                                        "FROM `".Database::GetPrefix()."resource`\n".
                                        "WHERE `id_bucket`=".((int)$bucketId)." AND\n".
                                        "    `http_response_code` IS NOT NULL AND\n".
                                        "    `http_response_code` != 200\n");

    if ($resources === false)
    {
        return -1;
    }

    return Database::GetResultAssoc($resources);
    */
}

function getResourcesCount($bucketId)
{
    $count = Database::Get()->query("SELECT COUNT(*) AS `result`\n".
                                    "FROM `".Database::GetPrefix()."resource`\n".
                                    "WHERE `id_bucket`=".((int)$bucketId));

    if ($count === false)
    {
        return -1;
    }

    $count = Database::GetResultAssoc($count);

    if (count($count) <= 0)
    {
        return -2;
    }

    return (int)$count[0]["result"];
}

function getLinksPaginated($bucketId, $offset, $limit)
{
    $links = Database::Get()->query("SELECT `id`,\n".
                                    "    `id_resource_source`,\n".
                                    "    `id_resource_target`,\n".
                                    "    `contexts`".
                                    "FROM `".Database::GetPrefix()."link`\n".
                                    "WHERE `id_bucket`=".((int)$bucketId)."\n".
                                    "ORDER BY `id`\n"/*.
                                    "LIMIT ".((int)$limit)." OFFSET ".((int)$offset)*/);

    if ($links === false)
    {
        return -1;
    }

    return Database::GetResultAssoc($links);
}

function getLinksCountInternal($bucketId)
{
    $count = Database::Get()->query("SELECT COUNT(*) AS `result`\n".
                                    "FROM `".Database::GetPrefix()."link`\n".
                                    "WHERE `id_bucket`=".((int)$bucketId)." AND\n".
                                    "    (`id_resource_source` IS NOT NULL AND\n".
                                    "     `id_resource_target` IS NOT NULL)");

    if ($count === false)
    {
        return -1;
    }

    $count = Database::GetResultAssoc($count);

    if (count($count) <= 0)
    {
        return -2;
    }

    return (int)$count[0]["result"];
}

function getCrawlPool($bucketId, $offset, $limit)
{
    $sql = "SELECT `id`,\n".
           "    `url`\n".
           "FROM `".Database::GetPrefix()."resource`\n".
           "WHERE `id_bucket`=".((int)$bucketId)." AND\n".
           "    `last_retrieval` IS NULL\n";

    if ((int)$offset >= 0)
    {
        $sql .= "ORDER BY `id`\n".
                "LIMIT ".((int)$limit)." OFFSET ".((int)$offset);
    }
    else
    {
        $sql .= "ORDER BY RAND()\n".
                "LIMIT ".((int)$limit);
    }

    $pool = Database::Get()->query($sql);

    if ($pool === false)
    {
        return -1;
    }

    $pool = Database::GetResultAssoc($pool);

    return $pool;
}

/**
 * @param[in] $url URL of the page the $links belong to. Can leave this
 *     null and pass initial URL(s) inside an array to parameter $links.
 *     Deliberately not creating a resource entry for $url if $url != null,
 *     in order to prevent accidental insertion of $links to a new/different
 *     URL than the one that would usually be desired per given $url!
 * @param[in] $links An array with an entry pro link found on the page
 *     identified by $url. Each entry is an associative array, containing
 *     the details for the link.
 * @retval 0 Success.
 * @retval -1 Error.
 * @retval -2 Resource not found.
 */
function updateRetrieval($bucketId,
                         $url,
                         $httpResponseCode,
                         $resourceHandle,
                         $links)
{
    $resource = null;

    if ($url != null)
    {
        // First, try to find the resource that got retrieved, and
        // learn + check some more details about it.

        $resource = Database::Get()->query("SELECT `id`,\n".
                                           "    `last_retrieval`,\n".
                                           "    `id_bucket`\n".
                                           "FROM `".Database::GetPrefix()."resource`\n".
                                           "WHERE `id_bucket`=".((int)$bucketId)." AND\n".
                                           "    `url` LIKE \"".Database::Get()->real_escape_string($url)."\"");

        if ($resource == false)
        {
            return -1;
        }

        $resource = Database::GetResultAssoc($resource);
        $resourceCount = count($resource);

        if ($resourceCount < 1)
        {
            return -2;
        }

        if ($resourceCount > 1)
        {
            // Wait, what? Duplicates?
        }

        $resource = $resource[0];
    }
    else
    {
        $resource = array("last_retrieval" => null);
    }

    $existingResources = array();
    $countLinks = count($links);

    // Allowing this if $url == null has the additional benefit that
    // the URL(s) pre-existing in $links get resolved to their IDs
    // in $existingLinkTargets, and skipped from insertion if these
    // should already happen to exist.
    if ($countLinks > 0)
    {
        // Identify already existing resources/URLs.

        /** @todo This can likely be optimized, as the query above already
          * determined which URLs/links/resources/connections exist from a
          * previous retrieval, so the ones to be deleted can be identified
          * by not being in $links. */

        $sql = "";

        for ($i = 0; $i < $countLinks; $i++)
        {
            $links[$i]["url"] = strtolower($links[$i]["url"]);

            if ($i > 0)
            {
                $sql .= " OR ";
            }

            $sql .= "`url` LIKE \"".Database::Get()->real_escape_string($links[$i]["url"])."\"";
        }

        $existingLinkTargets = Database::Get()->query("SELECT `id`,\n".
                                                      "    `url`\n".
                                                      "FROM `".Database::GetPrefix()."resource`\n".
                                                      "WHERE `id_bucket`=".$bucketId." AND (".$sql.")\n");

        if ($existingLinkTargets == false)
        {
            return -1;
        }

        $existingLinkTargets = Database::GetResultAssoc($existingLinkTargets);

        for ($i = 0, $max = count($existingLinkTargets); $i < $max; $i++)
        {
            $existingResources[$existingLinkTargets[$i]["url"]] = ((int)$existingLinkTargets[$i]["id"]);
        }
    }

    if (Database::Get()->begin_transaction() !== true)
    {
        return -1;
    }

    // Inserts new URLs.
    for ($i = 0; $i < $countLinks; $i++)
    {
        if (array_key_exists($links[$i]["url"], $existingResources) != true)
        {
            // This is very inperformant in comparison to multi-value insert, but I need
            // to get the AUTO_INCREMENT IDs, and maybe can't reliably/safely assume
            // squential order from last/prior to last/highest, or assign IDs myself here.
            // Maybe an URL could be an edge identifier (in table "links"), but is that fast?

            $result = Database::Get()->query("INSERT INTO `".Database::GetPrefix()."resource` (`id`,\n".
                                             "    `url`,\n".
                                             "    `last_retrieval`,\n".
                                             "    `http_response_code`,\n".
                                             "    `resource_handle`,\n".
                                             "    `id_bucket`)\n".
                                             "VALUES (NULL,\n".
                                             "    \"".Database::Get()->real_escape_string($links[$i]["url"])."\",\n".
                                             "    NULL,\n".
                                             "    NULL,\n".
                                             "    NULL,\n".
                                             "    ".(int)$bucketId.")");

            if ($result !== true)
            {
                Database::Get()->rollback();
                return -1;
            }

            if (Database::Get()->insert_id === NULL)
            {
                Database::Get()->rollback();
                return -1;
            }

            $existingResources[$links[$i]["url"]] = Database::Get()->insert_id;
        }
    }

    if ($resource["last_retrieval"] != null)
    {
        // Delete all outgoing connections (because some might be outdated).

        $result = Database::Get()->query("DELETE FROM `".Database::GetPrefix()."link`\n".
                                         "WHERE `id_resource_source`=".((int)$resource["id"])."\n");

        if ($result !== true)
        {
            Database::Get()->rollback();
            return -1;
        }
    }

    if ($url != null)
    {
        $resourceHandleEscaped = "NULL";

        if ($resourceHandle !== null)
        {
            $resourceHandleEscaped = "\"".Database::Get()->real_escape_string($resourceHandle)."\"";
        }

        $result = Database::Get()->query("UPDATE `".Database::GetPrefix()."resource`\n".
                                         "SET `last_retrieval`=UTC_TIMESTAMP(),\n".
                                         "    `http_response_code`=".((int)$httpResponseCode).",\n".
                                         "    `resource_handle`=".$resourceHandleEscaped."\n".
                                         "WHERE `id`=".((int)$resource["id"])."\n");

        if ($result !== true)
        {
            Database::Get()->rollback();
            return -1;
        }
    }

    if ($countLinks > 0 &&
        $url != null)
    {
        // Add all outgoing connections (because some might be new,
        // and all old ones were deleted above).

        $sql = "";

        for ($i = 0; $i < $countLinks; $i++)
        {
            $contexts = 0;

            for ($j = 0, $max2 = count($links[$i]["contexts"]); $j < $max2; $j++)
            {
                $context = (int)$links[$i]["contexts"][$j];

                if ($context !== RESOURCE_LINKCONTEXT_UNKNOWN &&
                    $context !== RESOURCE_LINKCONTEXT_REGULAR &&
                    $context !== RESOURCE_LINKCONTEXT_COMMENT &&
                    $context !== RESOURCE_LINKCONTEXT_SCRIPT)
                {
                    throw new Exception("Not implemented.");
                }

                $contexts = $contexts | $context;
            }

            if ($i > 0)
            {
                $sql .= ", ";
            }

            $sql .= "(NULL, ".
                     ((int)$resource["id"]).", ".
                     $existingResources[$links[$i]["url"]].", ".
                     $contexts.", ".
                     ((int)$bucketId).")";
        }

        $result = Database::Get()->query("INSERT INTO `".Database::GetPrefix()."link` (`id`,\n".
                                         "    `id_resource_source`,\n".
                                         "    `id_resource_target`,\n".
                                         "    `contexts`,\n".
                                         "    `id_bucket`)\n".
                                         "VALUES ".$sql);

        if ($result !== true)
        {
            Database::Get()->rollback();
            return -1;
        }
    }

    if (Database::Get()->commit() !== true)
    {
        Database::Get()->rollback();
        return -1;
    }

    return 0;
}

function getLinkBySourceTargetId($bucketId, $sourceId, $targetId)
{
    $link = Database::Get()->query("SELECT `id`,\n".
                                   "    `contexts`\n".
                                   "FROM `".Database::GetPrefix()."link`\n".
                                   "WHERE `id_resource_source`=".((int)$sourceId)." AND\n".
                                   "    `id_resource_target`=".((int)$targetId)." AND\n".
                                   "    `id_bucket`=".((int)$bucketId));

    if ($link === false)
    {
        return -1;
    }

    $link = Database::GetResultAssoc($link);
    $linkCount = count($link);

    if ($linkCount > 1)
    {
        // Wait, what? Duplicates?
        return -2;
    }

    return $link;
}

function resetResources($bucketId, $ids)
{
    if (count($ids) <= 0)
    {
        return 1;
    }

    $sql = "`id`=";

    {
        $first = true;

        foreach ($ids as $id)
        {
            if ($first == true)
            {
                $first = false;
            }
            else
            {
                $sql .= " OR `id`=";
            }

            $sql .= (int)$id;
        }
    }

    if (Database::Get()->begin_transaction() !== true)
    {
        return -1;
    }

    $result = Database::Get()->query("UPDATE `".Database::GetPrefix()."resource`\n".
                                     "SET `last_retrieval`=NULL,\n".
                                     "    `http_response_code`=NULL,\n".
                                     "    `resource_handle`=NULL\n".
                                     "WHERE `id_bucket`=".((int)$bucketId)." AND\n".
                                     "    ".$sql);

    if ($result !== true)
    {
        Database::Get()->rollback();
        return -2;
    }

    $sql = "`id_resource_source`=";

    {
        $first = true;

        foreach ($ids as $id)
        {
            if ($first == true)
            {
                $first = false;
            }
            else
            {
                $sql .= " OR `id_resource_source`=";
            }

            $sql .= (int)$id;
        }
    }

    $result = Database::Get()->query("DELETE FROM `".Database::GetPrefix()."link`\n".
                                     "WHERE `id_bucket`=".((int)$bucketId)." AND\n".
                                     "    ".$sql);

    if ($result !== true)
    {
        Database::Get()->rollback();
        return -3;
    }

    if (Database::Get()->commit() !== true)
    {
        Database::Get()->rollback();
        return -4;
    }

    return 0;
}



?>

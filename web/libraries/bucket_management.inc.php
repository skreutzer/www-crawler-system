<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/bucket_management.inc.php
 * @author Stephan Kreutzer
 * @since 2023-04-30
 */


require_once(dirname(__FILE__)."/database.inc.php");


function getBuckets()
{
    $buckets = Database::Get()->query("SELECT `id`,\n".
                                      "    `url`\n".
                                      "FROM `".Database::GetPrefix()."bucket`\n".
                                      "WHERE 1");

    if ($buckets === false)
    {
        return -1;
    }

    $buckets = Database::GetResultAssoc($buckets);

    return $buckets;
}

function getBucketById($id)
{
    $bucket = Database::Get()->query("SELECT `url`\n".
                                     "FROM `".Database::GetPrefix()."bucket`\n".
                                     "WHERE `id`=".((int)$id));

    if ($bucket === false)
    {
        return -1;
    }

    return Database::GetResultAssoc($bucket);
}

function insertBucket($startUrl)
{
    if (Database::Get()->begin_transaction() !== true)
    {
        return -1;
    }

    if (Database::Get()->query("INSERT INTO `".Database::GetPrefix()."bucket` (`id`,\n".
                               "    `url`)\n".
                               "VALUES (NULL, \"".Database::Get()->real_escape_string($startUrl)."\")") !== true)
    {
        Database::Get()->rollback();
        return -2;
    }

    $bucketId = Database::Get()->insert_id;

    if ($bucketId <= 0)
    {
        Database::Get()->rollback();
        return -3;
    }

    $filterId = insertBucketUrlFilter($bucketId, $startUrl);

    if ($filterId <= 0)
    {
        Database::Get()->rollback();
        return -4;
    }

    require_once(dirname(__FILE__)."/resource_management.inc.php");

    if (updateRetrieval($bucketId,
                        NULL,
                        NULL,
                        NULL,
                        array(array("url" => $startUrl))) !== 0)
    {
        Database::Get()->rollback();
        return -5;
    }

    if (Database::Get()->commit() !== true)
    {
        Database::Get()->rollback();
        return -6;
    }

    return $bucketId;
}

/**
 * @param $userId null for all.
 */
function getBucketsByUserId($userId)
{
    $sql = "";

    if ($userId === null)
    {
        $sql = "1";
    }
    else
    {
        $sql = "`".Database::GetPrefix()."bucket_user`.`id_user`=".(int)$userId;
    }

    $buckets = Database::Get()->query("SELECT `".Database::GetPrefix()."bucket`.`id` AS `bucket_id`,\n".
                                      "    `".Database::GetPrefix()."bucket`.`url` AS `bucket_url`\n".
                                      // Actually not wanted, was just for WHERE/filtering. Don't forget to add a leading comma.
                                      //"    `".Database::GetPrefix()."bucket_user`.`id_user` AS `bucket_user_id_user`\n".
                                      "FROM `".Database::GetPrefix()."bucket`\n".
                                      "LEFT JOIN `".Database::GetPrefix()."bucket_user` ON\n".
                                      "    `".Database::GetPrefix()."bucket`.`id` =\n".
                                      "    `".Database::GetPrefix()."bucket_user`.`id_bucket`\n".
                                      "WHERE ".$sql."\n".
                                      "GROUP BY `".Database::GetPrefix()."bucket`.`id`");

    if ($buckets === false)
    {
        return -1;
    }

    return Database::GetResultAssoc($buckets);
}

function checkBucketPermission($bucketId)
{
    require_once(dirname(__FILE__)."/user_management.inc.php");

    $bucketPermission = (((int)($_SESSION["user_role"])) === USER_ROLE_ADMIN);

    if ($bucketPermission === true)
    {
        return true;
    }

    $buckets = getBucketsByUserId((int)$_SESSION["user_id"]);

    if (is_array($buckets) !== true)
    {
        http_response_code(500);
        exit(1);
    }

    for ($i = 0, $max = count($buckets); $i < $max; $i++)
    {
        if ((int)$buckets[$i]["bucket_id"] === $bucketId)
        {
            return true;
        }
    }

    return false;
}

function getBucketUserMapping($bucketId)
{
    $mapping = Database::Get()->query("SELECT `id_user`\n".
                                      "FROM `".Database::GetPrefix()."bucket_user`\n".
                                      "WHERE `id_bucket`=".((int)$bucketId));

    if ($mapping === false)
    {
        return -1;
    }

    return Database::GetResultAssoc($mapping);
}

function updateBucketUserMapping($bucketId, $mapping)
{
    $mappingCount = count($mapping);

    $bucket = getBucketById((int)$bucketId);

    if (is_array($bucket) !== true)
    {
        return -2;
    }

    if (count($bucket) != 1)
    {
        return -3;
    }

    if ($mappingCount > 0)
    {
        $sql = "";

        for ($i = 0; $i < $mappingCount; $i++)
        {
            if ($i > 0)
            {
                $sql .= " OR ";
            }

            $sql .= "`id`=".(int)$mapping[$i];
        }

        $users = Database::Get()->query("SELECT `id`\n".
                                        "FROM `".Database::GetPrefix()."user`\n".
                                        "WHERE ".$sql);

        if ($users === false)
        {
            return -4;
        }

        $users = Database::GetResultAssoc($users);
        $usersCount = count($users);

        if ($usersCount != $mappingCount)
        {
            return -5;
        }

        for ($i = 0; $i < $mappingCount; $i++)
        {
            $found = false;

            for ($j = 0; $j < $usersCount; $j++)
            {
                if ((int)$mapping[$i] == (int)$users[$j]["id"])
                {
                    $found = true;
                    break;
                }
            }

            if ($found != true)
            {
                return -6;
            }
        }
    }

    if (Database::Get()->begin_transaction() !== true)
    {
        return -7;
    }

    $result = Database::Get()->query("DELETE FROM `".Database::GetPrefix()."bucket_user`\n".
                                     "WHERE `id_bucket`=".((int)$bucketId));

    if ($result !== true)
    {
        Database::Get()->rollback();
        return -8;
    }

    if ($mappingCount > 0)
    {
        $sql = "";

        for ($i = 0; $i < $mappingCount; $i++)
        {
            if ($i > 0)
            {
                $sql .= ", ";
            }

            $sql .= "(".((int)$bucketId).", ".((int)$mapping[$i]).")";
        }

        $result = Database::Get()->query("INSERT INTO `".Database::GetPrefix()."bucket_user` (`id_bucket`,\n".
                                         "    `id_user`)\n".
                                         "VALUES ".$sql);

        if ($result !== true)
        {
            Database::Get()->rollback();
            return -9;
        }
    }

    if (Database::Get()->commit() !== true)
    {
        Database::Get()->rollback();
        return -10;
    }

    return 0;
}

function getBucketUrlFilters($bucketId)
{
    $filters = Database::Get()->query("SELECT `id`,\n".
                                      "    `filter`\n".
                                      "FROM `".Database::GetPrefix()."bucket_url_filter`\n".
                                      "WHERE `id`=".((int)$bucketId));

    if ($filters === false)
    {
        return -1;
    }

    return Database::GetResultAssoc($filters);
}

function insertBucketUrlFilter($bucketId, $filter)
{
    if (Database::Get()->query("INSERT INTO `".Database::GetPrefix()."bucket_url_filter` (`id`,\n".
                               "    `filter`,\n".
                               "    `id_bucket`)\n".
                               "VALUES (NULL, \"".Database::Get()->real_escape_string($filter)."\", ".((int)$bucketId).")") !== true)
    {
        return -1;
    }

    $id = Database::Get()->insert_id;

    if ($id <= 0)
    {
        return -2;
    }

    return $id;
}

function getBucketUrlCountFilters($bucketId)
{
    $counts = Database::Get()->query("SELECT `".Database::GetPrefix()."url_count_filter`.`id` AS `url_count_filter_count_id`,\n".
                                     "    `".Database::GetPrefix()."url_count_filter`.`count_threshold` AS `url_count_filter_count_threshold`,\n".
                                     "    `".Database::GetPrefix()."resource`.`url` AS `resource_url`\n".
                                     "FROM `".Database::GetPrefix()."url_count_filter`\n".
                                     "LEFT JOIN `".Database::GetPrefix()."resource` ON\n".
                                     "    `".Database::GetPrefix()."url_count_filter`.`id_resource` =\n".
                                     "    `".Database::GetPrefix()."resource`.`id`\n".
                                     "WHERE `".Database::GetPrefix()."url_count_filter`.`id_bucket`=".((int)$bucketId));

    if ($counts === false)
    {
        return -1;
    }

    return Database::GetResultAssoc($counts);
}

/**
 * @param[in] $threshold Zero or negative to delete.
 */
function updateBucketUrlCountFilters($bucketId, $filterId, $threshold)
{
    if (checkBucketPermission($bucketId) !== true)
    {
        return -1;
    }

    if ((int)$threshold > 0)
    {
        $result = Database::Get()->query("UPDATE `".Database::GetPrefix()."url_count_filter`\n".
                                         "SET `count_threshold`=".((int)$threshold)."\n".
                                         "WHERE `id`=".((int)$filterId)."\n");

        if ($result === true)
        {
            return 0;
        }
        else
        {
            return -1;
        }
    }
    else
    {
        $result = Database::Get()->query("DELETE FROM `".Database::GetPrefix()."url_count_filter`\n".
                                         "WHERE `id`=".((int)$filterId)."\n");

        if ($result === true)
        {
            return 0;
        }
        else
        {
            return -1;
        }
    }
}

function insertBucketUrlCountFilters($bucketId, $url, $threshold)
{
    if ($threshold <= 0)
    {
        return 1;
    }

    if (checkBucketPermission($bucketId) !== true)
    {
        return -1;
    }

    require_once(dirname(__FILE__)."/resource_management.inc.php");

    $resource = getResourceByUrl($bucketId, $url, false);

    if (is_array($resource) !== true)
    {
        return -2;
    }

    if (count($resource) <= 0)
    {
        return -3;
    }

    if (Database::Get()->query("INSERT INTO `".Database::GetPrefix()."url_count_filter` (`id`,\n".
                               "    `id_resource`,\n".
                               "    `count_threshold`,\n".
                               "    `id_bucket`)\n".
                               "VALUES (NULL, ".((int)$resource[0]["id"]).", ".((int)$threshold).", ".((int)$bucketId).")") !== true)
    {
        return -4;
    }

    $id = Database::Get()->insert_id;

    if ($id <= 0)
    {
        return -5;
    }

    return $id;
}


?>

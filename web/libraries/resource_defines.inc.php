<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/libraries/resource_defines.inc.php
 * @author Stephan Kreutzer
 * @since 2023-03-26
 */

define("RESOURCE_LINKCONTEXT_UNKNOWN", 0);
define("RESOURCE_LINKCONTEXT_REGULAR", 1);
define("RESOURCE_LINKCONTEXT_COMMENT", 2);
define("RESOURCE_LINKCONTEXT_SCRIPT", 4);

?>

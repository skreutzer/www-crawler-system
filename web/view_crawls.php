<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/view_crawls.php
 * @author Stephan Kreutzer
 * @since 2023-03-20
 */


require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");


require_once("./libraries/bucket_management.inc.php");
require_once("./libraries/resource_management.inc.php");

require_once("./libraries/user_defines.inc.php");

$userId = (int)$_SESSION['user_id'];

if (((int)($_SESSION['user_role'])) === USER_ROLE_ADMIN)
{
    $userId = null;
}

$buckets = getBucketsByUserId($userId);

if (is_array($buckets) !== true)
{
    http_response_code(500);
    exit(1);
}

require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("view_crawls"));


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n".
     "        <ul>\n";

for ($i = 0, $max = count($buckets); $i < $max; $i++)
{
    $bucketId = (int)$buckets[$i]["bucket_id"];

    if (isset($_SESSION["bucket_stats"]) !== true)
    {
        $_SESSION["bucket_stats"] = array();
    }

    if (isset($_SESSION["bucket_stats"][$bucketId]) !== true)
    {
        // This may make the first visit very slow, if a user has many
        // buckets/projects on this instance.

        $resourceCount = getResourcesCount($bucketId);
        $linkCount = getLinksCountInternal($bucketId);

        if ($resourceCount >= 0)
        {
            $_SESSION["bucket_stats"][$bucketId] = array("count_resource" => $resourceCount,
                                                         "count_link" => $linkCount);
        }
    }

    echo "          <li>\n".
         "            ".htmlspecialchars($buckets[$i]["bucket_url"], ENT_XHTML | ENT_QUOTES, "UTF-8").": ".
         "<a href=\"./graph/graph-radialtree.html?bucket-id=".$bucketId."\">".LANG_LINKCAPTION_GRAPHRADIALTREE."</a>, ".
         "<a href=\"./graph/graph-grid.html?bucket-id=".$bucketId."\">".LANG_LINKCAPTION_GRAPHGRID."</a>, ".
         "<a href=\"./reports/changes.php?bucket-id=".$bucketId."\">".LANG_LINKCAPTION_CHANGEREPORT."</a>, ".
         "<a href=\"./reports/errors.php?bucket-id=".$bucketId."\">".LANG_LINKCAPTION_HTTPERRORCODES."</a>; ".
         LANG_TEXT_CSVEXPORT." ".
         "<a href=\"export.php?bucket-id=".$bucketId."&amp;source=1\">".LANG_LINKCAPTION_CSVEXPORT_NODES."</a>, ".
         "<a href=\"export.php?bucket-id=".$bucketId."&amp;source=2\">".LANG_LINKCAPTION_CSVEXPORT_EDGES."</a>, ".
         "<a href=\"./reports/changes_export.php?bucket-id=".$bucketId."\">".LANG_LINKCAPTION_CSVEXPORT_CHANGEREPORT."</a>, ".
         "<a href=\"./reports/errors_export.php?bucket-id=".$bucketId."\">".LANG_LINKCAPTION_CSVEXPORT_HTTPERRORCODES."</a>";

    if (isset($_SESSION["bucket_stats"][$bucketId]) === true)
    {
        echo ": ".
             sprintf(LANG_TEXT_STATS,
                     $_SESSION["bucket_stats"][$bucketId]["count_resource"],
                     $_SESSION["bucket_stats"][$bucketId]["count_link"]);
    }

    echo "\n".
         "          </li>\n";
}

echo "        </ul>\n".
     "        <div>\n".
     "          <a href=\"index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
     "        </div>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>

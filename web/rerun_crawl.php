<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/rerun_crawl.php
 * @author Stephan Kreutzer
 * @since 2023-05-12
 */


require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");


require_once("./libraries/user_defines.inc.php");

$userId = (int)$_SESSION['user_id'];

if (((int)($_SESSION['user_role'])) === USER_ROLE_ADMIN)
{
    $userId = null;
}

require_once("./libraries/change_management.inc.php");

if (isset($_GET["bucket-id"]) !== true)
{
    http_response_code(400);
    echo "'bucket-id' is missing.";
    exit(1);
}

$bucketId = (int)$_GET["bucket-id"];

require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("rerun_crawl"));


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n";

$changes = getChanges($bucketId);

if (count($changes) > 0)
{
    $reset = array();

    foreach ($changes as $change)
    {
        $action = (int)$change["link_change_action"];

        if ($action == CHANGE_ACTION_REMOVE ||
            $action == CHANGE_ACTION_ADD)
        {
            if ($change["link_change_id_resource_source"] == null)
            {
                // There shouldn't be change recommendations that originate from
                // an external page/site, as there's no control over these anyway.
                continue;
            }

            $reset[(int)$change["link_change_id_resource_source"]] = $change["resource_source_url"];
        }
    }

    require_once("./libraries/resource_management.inc.php");

    $result = resetResources($bucketId, array_keys($reset));

    if ($result == 0)
    {
        require_once("./libraries/change_management.inc.php");

        $result = clearChanges($bucketId);

        echo "        <p>\n".
             "          ".LANG_TEXT_URLSPREPARED."\n".
             "        </p>\n".
             "        <ul>\n";

        foreach ($reset as $id => $url)
        {
            echo "          <li>".htmlspecialchars($url, ENT_XHTML, "UTF-8")."</li>\n";
        }

        echo "        </ul>\n";
    }
    else
    {
        /** @todo Handle! */
    }
}
else
{
    echo "        <p>\n".
         "          ".LANG_TEXT_NOURLS."\n".
         "        </p>\n";
}

echo "        <div>\n".
     "          <a href=\"./crawler/index.xhtml?id=".$bucketId."\">".LANG_LINKCAPTION_CRAWLRUN."</a>\n".
     "        </div>\n".
     "        <div>\n".
     "          <a href=\"index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
     "        </div>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>

<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/api/change2.php
 * @author Stephan Kreutzer
 * @since 2023-05-05
 */


require_once("../libraries/https.inc.php");
require_once("../libraries/session.inc.php");
require_once("../libraries/bucket_management.inc.php");
require_once("../libraries/change_management.inc.php");


if ($_SERVER["REQUEST_METHOD"] === "GET")
{
    if (isset($_GET["bucket-id"]) !== true)
    {
        http_response_code(400);
        echo "'bucket-id' is missing.";
        exit(1);
    }

    $bucketId = (int)$_GET["bucket-id"];

    if (checkBucketPermission($bucketId) !== true)
    {
        http_response_code(403);
        exit(0);
    }

    $changes = getChanges($bucketId);

    if (is_array($changes) != true)
    {
        http_response_code(500);
        exit(-1);
    }

    http_response_code(200);
    header("Content-Type: application/json");

    echo "[";

    for ($i = 0, $max = count($changes); $i < $max; $i++)
    {
        if ($i > 0)
        {
            echo ",";
        }

        echo "{\"action\":";

        switch ((int)$changes[$i]["link_change_action"])
        {
        case CHANGE_ACTION_REMOVE:
            echo "\"remove\"";
            break;
        case CHANGE_ACTION_ADD:
            echo "\"add\"";
            break;
        default:
            http_response_code(500);
            exit(-1);
        }

        echo ",\"idResourceSource\":";

        if ($changes[$i]["link_change_id_resource_source"] != null)
        {
            echo (int)$changes[$i]["link_change_id_resource_source"];
        }
        else
        {
            echo "null";
        }

        echo ",\"idResourceTarget\":";

        if ($changes[$i]["link_change_id_resource_target"] != null)
        {
            echo (int)$changes[$i]["link_change_id_resource_target"];
        }
        else
        {
            echo "null";
        }

        if ($changes[$i]["link_change_id_resource_source"] == null)
        {
            echo ",\"url\":".json_encode($changes[$i]["resource_source_url"]);
        }
        else if ($changes[$i]["link_change_id_resource_target"] == null)
        {
            echo ",\"url\":".json_encode($changes[$i]["resource_target_url"]);
        }

        echo "}";
    }

    echo "]";

    exit(0);
}
else
{
    http_response_code(405);
    exit(1);
}


?>

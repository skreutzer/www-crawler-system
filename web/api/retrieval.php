<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/api/retrieval.php
 * @author Stephan Kreutzer
 * @since 2023-03-10
 */


require_once("../libraries/https.inc.php");
require_once("../libraries/session.inc.php");
require_once("../libraries/user_management.inc.php");

if (((int)($_SESSION["user_role"])) !== USER_ROLE_ADMIN)
{
    http_response_code(403);
    exit(0);
}


require_once("../libraries/resource_management.inc.php");

if ($_SERVER["REQUEST_METHOD"] === "POST")
{
    if (isset($_GET["bucket-id"]) !== true)
    {
        http_response_code(400);
        echo "'bucket-id' is missing.";
        exit(1);
    }

    $bucketId = (int)$_GET["bucket-id"];
    $payload = "";

    {
        $source = @fopen("php://input", "r");

        while (true)
        {
            $chunk = @fread($source, 1024);

            if ($chunk == false)
            {
                break;
            }

            $payload .= $chunk;
        }
    }

    $payload = json_decode($payload, true);

    if ($payload === false)
    {
        http_response_code(400);
        exit(1);
    }

    if (is_array($payload) != true)
    {
        http_response_code(400);
        exit(1);
    }

    if (isset($payload["url"]) != true)
    {
        http_response_code(400);
        echo "'url' is missing.";
        exit(1);
    }

    if (strlen($payload["url"]) <= 0)
    {
        $payload["url"] = null;
    }

    if (isset($payload["httpResponseCode"]) !== true)
    {
        http_response_code(400);
        echo "'httpResponseCode' is missing.";
        exit(1);
    }

    $httpResponseCode = (int)$payload["httpResponseCode"];

    if ($httpResponseCode == 0)
    {
        $httpResponseCode = null;
    }

    if (isset($payload["resourceHandle"]) != true)
    {
        http_response_code(400);
        echo "'resourceHandle' is missing.";
        exit(1);
    }

    $resourceHandle = $payload["resourceHandle"];

    if (strlen($resourceHandle) <= 0)
    {
        $resourceHandle = null;
    }

    if (isset($payload["links"]) != true)
    {
        http_response_code(400);
        echo "'links' is missing.";
        exit(1);
    }

    $links = $payload["links"];

    if (is_string($links) === true)
    {
        if (strlen($links) > 0)
        {
            http_response_code(400);
            echo "'links' is a string, but not an empty string.";
            exit(1);
        }

        $links = array();
    }
    else if (is_array($links) === true)
    {
        // Is OK.
    }
    else
    {
        http_response_code(400);
        echo "'links' is neither an array nor a string.";
        exit(1);
    }

    $result = updateRetrieval($bucketId,
                              $payload["url"],
                              $httpResponseCode,
                              $resourceHandle,
                              $links);

    if ($result == 0)
    {
        http_response_code(201);
        exit(0);
    }
    else if ($result == -1)
    {
        http_response_code(500);
        exit(-1);
    }
    else if ($result == -2)
    {
        http_response_code(404);
        exit(0);
    }
    else
    {
        http_response_code(500);
        exit(-1);
    }
}
else
{
    http_response_code(405);
    exit(1);
}



?>

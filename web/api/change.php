<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/api/change.php
 * @author Stephan Kreutzer
 * @since 2023-04-30
 */


require_once("../libraries/https.inc.php");
require_once("../libraries/session.inc.php");
require_once("../libraries/bucket_management.inc.php");
require_once("../libraries/change_management.inc.php");


if ($_SERVER["REQUEST_METHOD"] === "GET")
{
    if (isset($_GET["bucket-id"]) !== true)
    {
        http_response_code(400);
        echo "'bucket-id' is missing.";
        exit(1);
    }

    $bucketId = (int)$_GET["bucket-id"];

    if (checkBucketPermission($bucketId) !== true)
    {
        http_response_code(403);
        exit(0);
    }

    if (isset($_GET["action"]) !== true)
    {
        http_response_code(400);
        echo "'action' is missing.";
        exit(1);
    }

    $action = CHANGE_ACTION_UNKNOWN;

    switch ($_GET["action"])
    {
    case "get":
        $action = CHANGE_ACTION_GET;
        break;
    case "remove":
        $action = CHANGE_ACTION_REMOVE;
        break;
    case "remove-undo":
        $action = CHANGE_ACTION_REMOVEUNDO;
        break;
    case "add":
        $action = CHANGE_ACTION_ADD;
        break;
    case "add-undo":
        $action = CHANGE_ACTION_ADDUNDO;
        break;
    default:
        http_response_code(400);
        echo "Value for 'action' is not supported.";
        exit(1);
    }


    if ($action == CHANGE_ACTION_GET)
    {
        if (isset($_GET["resource-id"]) !== true)
        {
            http_response_code(400);
            echo "'resource-id' is missing.";
            exit(1);
        }

        $changes = getChangesByResourceId($bucketId, (int)$_GET["resource-id"]);

        if (is_array($changes) != true)
        {
            http_response_code(500);
            exit(-1);
        }

        http_response_code(200);
        header("Content-Type: application/json");

        echo "[";

        for ($i = 0, $max = count($changes); $i < $max; $i++)
        {
            if ($i > 0)
            {
                echo ",";
            }

            echo "{\"action\":";

            switch ((int)$changes[$i]["action"])
            {
            case CHANGE_ACTION_REMOVE:
                echo "\"remove\"";
                break;
            case CHANGE_ACTION_ADD:
                echo "\"add\"";
                break;
            default:
                http_response_code(500);
                exit(-1);
            }

            echo ",\"idResourceSource\":";

            if ($changes[$i]["id_resource_source"] != null)
            {
                echo (int)$changes[$i]["id_resource_source"];
            }
            else
            {
                echo "null";
            }

            echo ",\"idResourceTarget\":";

            if ($changes[$i]["id_resource_target"] != null)
            {
                echo (int)$changes[$i]["id_resource_target"];
            }
            else
            {
                echo "null";
            }

            if ($changes[$i]["id_resource_source"] == null)
            {
                echo ",\"url\":".json_encode($changes[$i]["url"]);
            }
            else if ($changes[$i]["id_resource_target"] == null)
            {
                echo ",\"url\":".json_encode($changes[$i]["url"]);
            }

            echo "}";
        }

        echo "]";

        exit(0);
    }
    else if ($action == CHANGE_ACTION_REMOVE)
    {
        if (isset($_GET["resource-id-source"]) !== true)
        {
            http_response_code(400);
            echo "'resource-id-source' is missing.";
            exit(1);
        }

        $idSource = (int)$_GET["resource-id-source"];

        if (isset($_GET["resource-id-target"]) !== true)
        {
            http_response_code(400);
            echo "'resource-id-target' is missing.";
            exit(1);
        }

        $idTarget = (int)$_GET["resource-id-target"];

        require_once("../libraries/resource_management.inc.php");

        $link = getLinkBySourceTargetId($bucketId, $idSource, $idTarget);

        if (is_array($link) !== true)
        {
            http_response_code(500);
            exit(-1);
        }

        if (count($link) < 1)
        {
            http_response_code(404);
            exit(0);
        }

        $link = $link[0];

        $result = changeRemove($bucketId, $idSource, $idTarget);

        if ($result <= 0)
        {
            if ($result == -2 ||
                $result == -3)
            {
                http_response_code(400);
                exit(1);
            }
            else
            {
                http_response_code(500);
                exit(-1);
            }
        }

        http_response_code(201);
        exit(0);
    }
    else if ($action == CHANGE_ACTION_REMOVEUNDO)
    {
        if (isset($_GET["resource-id-source"]) !== true)
        {
            http_response_code(400);
            echo "'resource-id-source' is missing.";
            exit(1);
        }

        $idSource = (int)$_GET["resource-id-source"];

        if (isset($_GET["resource-id-target"]) !== true)
        {
            http_response_code(400);
            echo "'resource-id-target' is missing.";
            exit(1);
        }

        $idTarget = (int)$_GET["resource-id-target"];

        $result = changeRemoveUndo($bucketId, $idSource, $idTarget);

        if ($result != 0)
        {
            http_response_code(500);
            exit(-1);
        }

        http_response_code(201);
        exit(0);
    }
    else if ($action == CHANGE_ACTION_ADD)
    {
        if (isset($_GET["url"]) !== true)
        {
            http_response_code(400);
            echo "'url' is missing.";
            exit(1);
        }

        $idSource = null;
        $idTarget = null;

        if (isset($_GET["resource-id-source"]) === true)
        {
            $idSource = (int)$_GET["resource-id-source"];
        }

        if (isset($_GET["resource-id-target"]) === true)
        {
            $idTarget = (int)$_GET["resource-id-target"];
        }

        if ($idSource === null &&
            $idTarget === null)
        {
            http_response_code(400);
            echo "No 'resource-id-source' and no 'resource-id-target'.";
            exit(1);
        }

        if ($idSource !== null &&
            $idTarget !== null)
        {

        }

        $urlToIdResolve = null;

        $result = changeAdd($bucketId, $idSource, $idTarget, $_GET["url"], $urlToIdResolve);

        if ($result <= 0)
        {
            http_response_code(500);
            exit(-1);
        }

        http_response_code(201);
        header("Content-Type: application/json");

        if ($urlToIdResolve !== null)
        {
            echo "{\"urlResourceId\":".((int)$urlToIdResolve)."}";
        }
        else
        {
            echo "{}";
        }

        exit(0);
    }
    else if ($action == CHANGE_ACTION_ADDUNDO)
    {
        $url = null;
        $idSource = null;
        $idTarget = null;

        if (isset($_GET["resource-id-source"]) === true)
        {
            $idSource = (int)$_GET["resource-id-source"];
        }

        if (isset($_GET["resource-id-target"]) === true)
        {
            $idTarget = (int)$_GET["resource-id-target"];
        }

        if (isset($_GET["url"]) === true)
        {
            $url = $_GET["url"];
        }

        if ($idSource !== null &&
            $idTarget !== null)
        {
            $url = null;
        }
        else
        {
            if ($idSource === null &&
                $idTarget === null)
            {
                http_response_code(400);
                echo "Neither 'resource-id-source' nor 'resource-id-target' is set.";
                exit(1);
            }

            if ($url === null)
            {
                http_response_code(400);
                echo "'url' is missing.";
                exit(1);
            }
        }

        $result = changeAddUndo($bucketId, $idSource, $idTarget, $url);

        if ($result != 0)
        {
            http_response_code(500);
            exit(-1);
        }

        http_response_code(201);
        exit(0);
    }
}
else
{
    http_response_code(405);
    exit(1);
}


?>

<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/api/bucket_url_filter.php
 * @author Stephan Kreutzer
 * @since 2023-03-15
 */


require_once("../libraries/https.inc.php");
require_once("../libraries/session.inc.php");
require_once("../libraries/bucket_management.inc.php");


if ($_SERVER["REQUEST_METHOD"] === "GET")
{
    if (isset($_GET["bucket-id"]) !== true)
    {
        http_response_code(400);
        echo "'bucket-id' is missing.";
        exit(1);
    }

    $bucketId = (int)$_GET["bucket-id"];

    if (checkBucketPermission($bucketId) !== true)
    {
        http_response_code(403);
        exit(0);
    }

    $filters = getBucketUrlFilters($bucketId);

    if (is_array($filters) != true)
    {
        http_response_code(500);
        exit(-1);
    }

    http_response_code(200);
    header("Content-Type: application/json");
    echo "[";

    for ($i = 0, $max = count($filters); $i < $max; $i++)
    {
        if ($i > 0)
        {
            echo ",";
        }

        echo "{\"url\":".json_encode($filters[$i]["filter"])."}";
    }

    echo "]";
}
else if ($_SERVER["REQUEST_METHOD"] === "POST")
{
    if (isset($_GET["bucket-id"]) !== true)
    {
        http_response_code(400);
        echo "'bucket-id' is missing.";
        exit(1);
    }

    require_once("../libraries/user_management.inc.php");

    if (((int)($_SESSION["user_role"])) !== USER_ROLE_ADMIN)
    {
        http_response_code(403);
        exit(0);
    }

    $bucketId = (int)$_GET["bucket-id"];
    $payload = "";

    {
        $source = @fopen("php://input", "r");

        while (true)
        {
            $chunk = @fread($source, 1024);

            if ($chunk == false)
            {
                break;
            }

            $payload .= $chunk;
        }
    }

    $payload = json_decode($payload, true);

    if ($payload === false)
    {
        http_response_code(400);
        exit(1);
    }

    if (is_array($payload) != true)
    {
        http_response_code(400);
        exit(1);
    }

    if (isset($payload["filter"]) != true)
    {
        http_response_code(400);
        echo "'filter' is missing.";
        exit(1);
    }

    if (strlen($payload["filter"]) <= 0)
    {
        http_response_code(400);
        echo "'filter' is empty.";
        exit(1);
    }

    $result = insertBucketUrlFilter($bucketId, $payload["filter"]);

    if ($result > 0)
    {
        http_response_code(201);
        exit(0);
    }
    else
    {
        http_response_code(500);
        exit(-1);
    }
}
else
{
    http_response_code(405);
    exit(1);
}


?>

<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/api/url_count_filter.php
 * @author Stephan Kreutzer
 * @since 2023-03-22
 */


require_once("../libraries/https.inc.php");
require_once("../libraries/session.inc.php");
require_once("../libraries/bucket_management.inc.php");
require_once("../libraries/resource_management.inc.php");


if ($_SERVER["REQUEST_METHOD"] === "GET")
{
    if (isset($_GET["bucket-id"]) !== true)
    {
        http_response_code(400);
        echo "'bucket-id' is missing.";
        exit(1);
    }

    $bucketId = (int)$_GET["bucket-id"];

    if (checkBucketPermission($bucketId) !== true)
    {
        http_response_code(403);
        exit(0);
    }

    $counts = getBucketUrlCountFilters($bucketId);

    if (is_array($counts) != true)
    {
        http_response_code(500);
        exit(-1);
    }

    http_response_code(200);
    header("Content-Type: application/json");
    echo "[";

    for ($i = 0, $max = count($counts); $i < $max; $i++)
    {
        if ($i > 0)
        {
            echo ",";
        }

        echo "{\"url\":".json_encode($counts[$i]["resource_url"]).",".
              "\"threshold\":".((int)$counts[$i]["url_count_filter_count_threshold"])."}";
    }

    echo "]";
}
else
{
    http_response_code(405);
    exit(1);
}


?>

<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/api/resource.php
 * @author Stephan Kreutzer
 * @since 2023-01-25
 */


require_once("../libraries/https.inc.php");
require_once("../libraries/session.inc.php");
require_once("../libraries/bucket_management.inc.php");
require_once("../libraries/resource_management.inc.php");
require_once("../libraries/resource_defines.inc.php");


if ($_SERVER["REQUEST_METHOD"] === "GET")
{
    if (isset($_GET["bucket-id"]) !== true)
    {
        http_response_code(400);
        echo "'bucket-id' is missing.";
        exit(1);
    }

    $bucketId = (int)$_GET["bucket-id"];

    if (checkBucketPermission($bucketId) !== true)
    {
        http_response_code(403);
        exit(0);
    }

    if (isset($_GET["crawl-pool"]) == true)
    {
        /** @todo This might become its own, separate endpoint? */

        $pool = null;

        if (isset($_GET["offset"]) === true)
        {
            $pool = getCrawlPool($bucketId, (int)$_GET["offset"], 50);
        }
        else
        {
            $pool = getCrawlPool($bucketId, -1, 50);
        }

        if (is_array($pool) != true)
        {
            http_response_code(500);
            exit(-1);
        }

        http_response_code(200);
        header("Content-Type: application/json");

        echo "{\"pool\":[";

        for ($i = 0, $max = count($pool); $i < $max; $i++)
        {
            if ($i > 0)
            {
                echo ",";
            }

            echo json_encode($pool[$i]["url"]);
        }

        echo "]}";

        exit(0);
    }
    else
    {
        if (isset($_GET["url"]) !== true &&
            isset($_GET["id"]) !== true)
        {
            http_response_code(400);
            echo "Both 'url' and 'id' are missing.";
            exit(1);
        }
        else
        {
            $resource = null;

            if (isset($_GET["id"]) === true)
            {
                $resource = getResource($bucketId, true, (int)$_GET["id"], true);
            }
            else if (isset($_GET["url"]) === true)
            {
                $resource = getResource($bucketId, false, $_GET["url"], true);
            }
            else
            {
                http_response_code(500);
                exit(1);
            }

            if (is_array($resource) !== true)
            {
                http_response_code(500);
                exit(-1);
            }

            if (count($resource) <= 0)
            {
                http_response_code(404);
                exit(0);
            }

            $resource = $resource[0];

            http_response_code(200);
            header("Content-Type: application/json");

            echo "{\"id\":".((int)$resource["id"]).",".
                  "\"url\":".json_encode($resource["url"]).",".
                  "\"links\":{\"incoming\":[";

            for ($i = 0, $max = count($resource["links"]["incoming"]); $i < $max; $i++)
            {
                if ($i > 0)
                {
                    echo ",";
                }

                echo "{\"id\":".((int)$resource["links"]["incoming"][$i]["id"]).",".
                      "\"idNode\":".((int)$resource["links"]["incoming"][$i]["id-node"]).",".
                      "\"url\":".json_encode($resource["links"]["incoming"][$i]["url"]).",".
                      "\"isRegular\":";

                $contexts = (int)$resource["links"]["incoming"][$i]["contexts"];

                if (($contexts & RESOURCE_LINKCONTEXT_REGULAR) == RESOURCE_LINKCONTEXT_REGULAR)
                {
                    echo "true";
                }
                else
                {
                    echo "false";
                }

                echo ",\"inComment\":";

                if (($contexts & RESOURCE_LINKCONTEXT_COMMENT) == RESOURCE_LINKCONTEXT_COMMENT)
                {
                    echo "true";
                }
                else
                {
                    echo "false";
                }

                echo ",\"inScript\":";

                if (($contexts & RESOURCE_LINKCONTEXT_SCRIPT) == RESOURCE_LINKCONTEXT_SCRIPT)
                {
                    echo "true";
                }
                else
                {
                    echo "false";
                }

                echo "}";
            }

            echo "],\"outgoing\":[";

            for ($i = 0, $max = count($resource["links"]["outgoing"]); $i < $max; $i++)
            {
                if ($i > 0)
                {
                    echo ",";
                }

                echo "{\"id\":".((int)$resource["links"]["outgoing"][$i]["id"]).",".
                      "\"idNode\":".((int)$resource["links"]["outgoing"][$i]["id-node"]).",".
                      "\"url\":".json_encode($resource["links"]["outgoing"][$i]["url"]).",".
                      "\"isRegular\":";

                $contexts = (int)$resource["links"]["outgoing"][$i]["contexts"];

                if (($contexts & RESOURCE_LINKCONTEXT_REGULAR) == RESOURCE_LINKCONTEXT_REGULAR)
                {
                    echo "true";
                }
                else
                {
                    echo "false";
                }

                echo ",\"inComment\":";

                if (($contexts & RESOURCE_LINKCONTEXT_COMMENT) == RESOURCE_LINKCONTEXT_COMMENT)
                {
                    echo "true";
                }
                else
                {
                    echo "false";
                }

                echo ",\"inScript\":";

                if (($contexts & RESOURCE_LINKCONTEXT_SCRIPT) == RESOURCE_LINKCONTEXT_SCRIPT)
                {
                    echo "true";
                }
                else
                {
                    echo "false";
                }

                echo "}";
            }

            echo "]}}";
        }
    }
}
else
{
    http_response_code(405);
    exit(1);
}


?>

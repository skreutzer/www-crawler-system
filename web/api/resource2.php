<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/api/resource2.php
 * @author Stephan Kreutzer
 * @since 2023-03-28
 */


require_once("../libraries/https.inc.php");
require_once("../libraries/session.inc.php");
require_once("../libraries/bucket_management.inc.php");
require_once("../libraries/resource_management.inc.php");
require_once("../libraries/resource_defines.inc.php");


if ($_SERVER["REQUEST_METHOD"] === "GET")
{
    if (isset($_GET["bucket-id"]) !== true)
    {
        http_response_code(400);
        echo "'bucket-id' is missing.";
        exit(1);
    }

    $bucketId = (int)$_GET["bucket-id"];

    if (checkBucketPermission($bucketId) !== true)
    {
        http_response_code(403);
        exit(0);
    }

    /** @todo 404 if bucket with this ID doesn't exist! */

    if (isset($_GET["source"]) !== true)
    {
        http_response_code(400);
        echo "'source' is missing.";
        exit(1);
    }

    if (isset($_GET["offset"]) !== true)
    {
        http_response_code(400);
        echo "'offset' is missing.";
        exit(1);
    }

    $offset = (int)$_GET["offset"];

    if ($offset < 0)
    {
        $offset = 0;
    }

    $source = $_GET["source"];

    if ($source === "resource")
    {
        $resources = getResourcesPaginated($bucketId, $offset, 50);

        if (is_array($resources) !== true)
        {
            http_response_code(500);
            exit(-1);
        }

        http_response_code(200);
        header("Content-Type: application/json");

        echo "{\"resources\":[";

        for ($i = 0, $max = count($resources); $i < $max; $i++)
        {
            if ($i > 0)
            {
                echo ",";
            }

            echo "{\"id\":".((int)$resources[$i]["id"]).",".
                  "\"url\":".json_encode($resources[$i]["url"])."}";
        }

        echo "]}";

        exit(0);
    }
    else if ($source === "link")
    {
        $links = getLinksPaginated($bucketId, $offset, 50);

        if (is_array($links) !== true)
        {
            http_response_code(500);
            exit(-1);
        }

        http_response_code(200);
        header("Content-Type: application/json");

        echo "{\"links\":[";

        for ($i = 0, $max = count($links); $i < $max; $i++)
        {
            if ($i > 0)
            {
                echo ",";
            }

            echo "{\"id\":".((int)$links[$i]["id"]).",".
                  "\"idResourceSource\":".((int)$links[$i]["id_resource_source"]).",".
                  "\"idResourceTarget\":".((int)$links[$i]["id_resource_target"]).",".
                  "\"isRegular\":";

            $contexts = (int)$links[$i]["contexts"];

            if (($contexts & RESOURCE_LINKCONTEXT_REGULAR) == RESOURCE_LINKCONTEXT_REGULAR)
            {
                echo "true";
            }
            else
            {
                echo "false";
            }

            echo ",\"inComment\":";

            if (($contexts & RESOURCE_LINKCONTEXT_COMMENT) == RESOURCE_LINKCONTEXT_COMMENT)
            {
                echo "true";
            }
            else
            {
                echo "false";
            }

            echo ",\"inScript\":";

            if (($contexts & RESOURCE_LINKCONTEXT_SCRIPT) == RESOURCE_LINKCONTEXT_SCRIPT)
            {
                echo "true";
            }
            else
            {
                echo "false";
            }

            echo "}";
        }

        echo "]}";

        exit(0);
    }
    else
    {
        http_response_code(400);
        echo "'source' isn't 'resource' nor 'link'.";
        exit(1);
    }

    throw new Exception("Not implemented.");
}
else
{
    http_response_code(405);
    exit(1);
}


?>

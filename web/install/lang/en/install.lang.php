<?php
/* Copyright (C) 2013-2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/install/lang/en/install.lang.php
 * @author Stephan Kreutzer
 * @since 2013-09-14
 */



define("LANG_PAGETITLE", "Installation");

// -- Step 0 -------------------------------------------------------------------

define("LANG_STEP0_HEADER", "Installation");
define("LANG_STEP0_INTROTEXT", "You’re about to install the crawler system.");
define("LANG_STEP0_PROCEEDTEXT", "Continue");

// -- Step 1 -------------------------------------------------------------------

define("LANG_STEP1_HEADER", "License Agreement");
define("LANG_STEP1_PROCEEDTEXT", "Agree");

// -- Step 2 -------------------------------------------------------------------

define("LANG_STEP2_HEADER", "Database Settings");
define("LANG_STEP2_REQUIREMENTS", "This software requires a running MySQL database server. Please fill in the database connection settings. They’ll get written into the file <tt>\$/libraries/database_connect.inc.php</tt> of the software installation directory.");
define("LANG_STEP2_HOSTDESCRIPTION", "Address of the database server");
define("LANG_STEP2_USERNAMEDESCRIPTION", "Database username");
define("LANG_STEP2_PASSWORDDESCRIPTION", "Password of that database user");
define("LANG_STEP2_DATABASENAMEDESCRIPTION", "Name of the database which will hold the tables");
define("LANG_STEP2_TABLEPREFIXDESCRIPTION", "Prefix for table names (could be empty if no name collusions are expected – prefix ends usually with an underscore '_')");
define("LANG_STEP2_SAVETEXT", "Save settings");
define("LANG_STEP2_EDITTEXT", "Edit settings");
define("LANG_STEP2_PROCEEDTEXT", "Confirm settings");
define("LANG_STEP2_DBCONNECTSUCCEEDED", "Connection to the database was successfully established!");
define("LANG_STEP2_DBCONNECTFAILED", "Connection to the database couldn’t be established. Error description: ");
define("LANG_STEP2_DBCONNECTFAILEDNOERRORINFO", "No error details!");
define("LANG_STEP2_DATABASECONNECTFILECREATEFAILED", "The attempt to create <tt>\$/libraries/database_connect.inc.php</tt> failed!");
define("LANG_STEP2_DATABASECONNECTFILEISWRITABLE", "<tt>\$/libraries/database_connect.inc.php</tt> is writable!");
define("LANG_STEP2_DATABASECONNECTFILEISNTWRITABLE", "<tt>\$/libraries/database_connect.inc.php</tt> isn’t writable! Either there are no write permissions granted for the directory <tt>\$/libraries/</tt>, so that the file can’t be created, or the file <tt>\$/libraries/database_connect.inc.php</tt> does already exist, but permissions for writing the file aren’t sufficient. You might have to enable write permission manually on the server for the directory <tt>\$/libraries/</tt> and/or for the file <tt>\$/libraries/database_connect.inc.php</tt> (if already existing) with a FTP program (CHMOD command) or via remote connection. Don’t forget to reset the permissions after the installation is completed.");
define("LANG_STEP2_DATABASECONNECTFILEWRITABLEOPENFAILED", "<tt>\$/libraries/database_connect.inc.php</tt> seemed to be writable, but opening the file failed!");
define("LANG_STEP2_DATABASECONNECTFILEWRITEFAILED", "<tt>\$/libraries/database_connect.inc.php</tt> seemed to be writable, was successfully opened, but can’t actually be written!");
define("LANG_STEP2_DATABASECONNECTFILEWRITESUCCEEDED", "Writing <tt>\$/libraries/database_connect.inc.php</tt> succeeded!");
define("LANG_STEP2_DATABASECONNECTFILEISNTREADABLE", "<tt>\$/libraries/database_connect.inc.php</tt> isn’t readable! Either there are no read permissions granted for the directory <tt>\$/libraries/</tt>, so that the file can’t be accessed, or the file <tt>\$/libraries/database_connect.inc.php</tt> itself lacks read permission. You might have to enable read permission manually on the server for the directory <tt>\$/libraries/</tt> and/or for the file <tt>\$/libraries/database_connect.inc.php</tt> with a FTP program (CHMOD command) or another file transfer program. Don’t forget to reset the permissions after the installation is completed.");
define("LANG_STEP2_DATABASECONNECTFILEISREADABLE", "Written <tt>\$/libraries/database_connect.inc.php</tt> is readable!");
define("LANG_STEP2_DATABASECONNECTFILEDOESNTEXIST", "<tt>\$/libraries/database_connect.inc.php</tt> doesn’t exist!");
define("LANG_STEP2_STORAGEDIRECTORYFILEWRITEFAILED", "Write attempt in directory <tt>\$/crawler/storage/</tt> failed! Please grant write permission for the directory on the server. You might have to enable write permission manually with a FTP program (CHMOD command) or another file transfer program.");
define("LANG_STEP2_STORAGEDIRECTORYFILEWRITABLEOPENFAILED", "Attempt to open the directory <tt>\$/crawler/storage/</tt> in write mode failed! Please grant write permission for the directory on the server. You might have to enable write permission manually with a FTP program (CHMOD command) or another file transfer program.");
define("LANG_STEP2_STORAGEDIRECTORYFILEISNTWRITABLE", "No write permission for directory <tt>\$/crawler/storage/</tt>! Please grant write permission for the directory on the server. You might have to enable write permission manually with a FTP program (CHMOD command) or another file transfer program.");

// -- Step 3 -------------------------------------------------------------------

define("LANG_STEP3_HEADER", "Initialization");
define("LANG_STEP3_INITIALIZETEXT", "Initialize");
define("LANG_STEP3_INITIALIZATIONDESCRIPTION", "The software is about to be initialized. This includes the creation of the tables in the database.");
define("LANG_STEP3_CHECKBOXDESCRIPTIONDROPEXISTINGTABLES", "Delete tables that already exist (warning: tables can’t be recovered!)");
define("LANG_STEP3_CHECKBOXDESCRIPTIONKEEPEXISTINGTABLES", "Only create tables, if not already existing (will keep existing tables without change)");
define("LANG_STEP3_DBOPERATIONFAILED", "Database operation failed. Error description: ");
define("LANG_STEP3_DBOPERATIONFAILEDNOERRORINFO", "No error details!");
define("LANG_STEP3_DBOPERATIONSUCCEEDED", "Initialization was successful!");
define("LANG_STEP3_COMPLETETEXT", "Complete");

// -- Step 4 -------------------------------------------------------------------

define("LANG_STEP4_HEADER", "Configuration");

define("LANG_STEP4_USERINITIALIZATIONDESCRIPTION", "Please create the first user. This user is also the administrator.");
define("LANG_STEP4_USERNAMEDESCRIPTION", "User name");
define("LANG_STEP4_PASSWORDDESCRIPTION", "Password");
define("LANG_STEP4_EMAILDESCRIPTION", "E-mail address");
define("LANG_STEP4_BUTTONSAVECAPTION", "Save");
define("LANG_STEP4_DBOPERATIONSUCCEEDED", "User successfully created.");
define("LANG_STEP4_PROCEEDTEXT", "Complete");
define("LANG_STEP4_DBOPERATIONFAILED", "Database operation failed.");

// -- Step 5 -------------------------------------------------------------------

define("LANG_STEP5_HEADER", "Complete!");
define("LANG_STEP5_COMPLETETEXT", "Installation succeeded! The installation routine will try to delete itself. If it can’t succeed (login on the main page won’t be possible then, instead, you’ll get forwarded to the installation again), you have to delete the directory <tt>\$/install/</tt> manually, or at least the file <tt>\$/install/install.php</tt>. Then the login form should be accessible.");
define("LANG_STEP5_EXITTEXT", "Exit");



?>

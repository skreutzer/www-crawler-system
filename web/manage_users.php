<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/manage_users.php
 * @author Stephan Kreutzer
 * @since 2023-03-20
 */


require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");

require_once("./libraries/user_management.inc.php");


if (((int)($_SESSION['user_role'])) !== USER_ROLE_ADMIN)
{
    http_response_code(403);
    exit(1);
}


if (isset($_POST["name"]) === true &&
    isset($_POST["password"]) === true)
{
    if (strlen($_POST["name"]) <= 0)
    {
    
    }

    if (strlen($_POST["password"]) <= 0)
    {
    
    }

    $result = insertNewUser($_POST["name"], $_POST["password"], USER_ROLE_USER);

    if ($result <= 0)
    {
    
    }
}


require_once("./libraries/languagelib.inc.php");
require_once(getLanguageFile("manage_users"));


echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"".getCurrentLanguage()."\" lang=\"".getCurrentLanguage()."\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <title>".LANG_PAGETITLE."</title>\n".
     "    <link rel=\"stylesheet\" type=\"text/css\" href=\"mainstyle.css\"/>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div class=\"mainbox\">\n".
     "      <div class=\"mainbox_header\">\n".
     "        <h1 class=\"mainbox_header_h1\">".LANG_HEADER."</h1>\n".
     "      </div>\n".
     "      <div class=\"mainbox_body\">\n";

$users = getUsers();

if (is_array($users) === true)
{
    echo "        <table>\n".
         "          <thead>\n".
         "            <tr>\n".
         "              <th>".LANG_TABLEHEADER_USERNAME."</th>\n".
         "              <th>".LANG_TABLEHEADER_ACTIONS."</th>\n".
         "            </tr>\n".
         "          </thead>\n".
         "          <tfoot>\n".
         "            <tr>\n".
         "              <td colspan=\"2\">\n".
         "                <form method=\"post\" action=\"manage_users.php\">\n".
         "                  <fieldset>\n".
         "                    <label for=\"name\">".LANG_FORMLABEL_USERNAME."</label>\n".
         "                    <input type=\"text\" name=\"name\" id=\"name\" required=\"required\"/>\n".
         "                    <label for=\"password\">".LANG_FORMLABEL_USERPASSWORD."</label>\n".
         "                    <input type=\"text\" name=\"password\" id=\"password\" required=\"required\"/>\n".
         "                    <input type=\"submit\" value=\"".LANG_FORMLABEL_CREATE."\"/>\n".
         "                  </fieldset>\n".
         "                </form>\n".
         "              </td>\n".
         "            </tr>\n".
         "          </tfoot>\n".
         "          <tbody>\n";

    foreach ($users as $user)
    {
        echo "            <tr>\n".
             "              <td>".htmlspecialchars($user["name"], ENT_XHTML, "UTF-8")."</td>\n".
             "              <td></td>\n".
             "            </tr>\n";
    }

    echo "          </tbody>\n".
         "        </table>\n";
}
else
{

}

echo "        <div>\n".
     "          <a href=\"index.php\">".LANG_LINKCAPTION_MAINPAGE."</a>\n".
     "        </div>\n".
     "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n".
     "\n";


?>

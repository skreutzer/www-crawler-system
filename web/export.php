<?php
/* Copyright (C) 2023 Stephan Kreutzer
 *
 * This file is part of www-crawler-system.
 *
 * www-crawler-system is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * www-crawler-system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with www-crawler-system. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @author Stephan Kreutzer
 * @since 2023-03-16
 */


require_once("./libraries/https.inc.php");
require_once("./libraries/session.inc.php");
require_once("./libraries/database.inc.php");
require_once("./libraries/resource_defines.inc.php");
require_once("./libraries/csv_utilities.inc.php");


if (isset($_GET["bucket-id"]) != true)
{
    http_response_code(400);
    echo "'bucket-id' is missing.";
    exit(1);
}

if (isset($_GET["source"]) != true)
{
    http_response_code(400);
    echo "'source' is missing.";
    exit(1);
}

$bucketId = (int)$_GET["bucket-id"];
$source = (int)$_GET["source"];

if ($source === 1)
{
    $resources = Database::Get()->query("SELECT `id`,\n".
                                        "    `url`,\n".
                                        "    `last_retrieval`,\n".
                                        "    `http_response_code`\n".
                                        "FROM `".Database::GetPrefix()."resource`\n".
                                        "WHERE `id_bucket`=".((int)$bucketId));

    if ($resources === false)
    {
        http_response_code(500);
        exit(-1);
    }

    $resources = Database::GetResultAssoc($resources);
    $resourcesCount = count($resources);

    http_response_code(200);
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=\"".((int)$bucketId)."_resources.csv\"");

    echo "\"id\",\"url\",\"last_retrieval_utc\",\"http_response_code\"\r\n";

    for ($i = 0; $i < $resourcesCount; $i++)
    {
        echo "\"".((int)$resources[$i]["id"])."\",".
             "\"".escapeCsvString($resources[$i]["url"])."\",";

        if ($resources[$i]["url"] != null)
        {
            echo "\"".escapeCsvString($resources[$i]["last_retrieval"])."\",";
        }
        else
        {
            echo "\"\",";
        }

        if ($resources[$i]["http_response_code"] != null)
        {
            echo "\"".((int)$resources[$i]["http_response_code"])."\"";
        }
        else
        {
            echo "\"\"";
        }

        echo "\r\n";
    }
}
else if ($source === 2)
{
    $links = Database::Get()->query("SELECT `id`,\n".
                                    "    `id_resource_source`,\n".
                                    "    `id_resource_target`,\n".
                                    "    `contexts`\n".
                                    "FROM `".Database::GetPrefix()."link`\n".
                                    "WHERE `id_bucket`=".((int)$bucketId));

    if ($links === false)
    {
        http_response_code(500);
        exit(-1);
    }

    $links = Database::GetResultAssoc($links);
    $linksCount = count($links);

    http_response_code(200);
    header("Content-Type: text/csv");
    header("Content-Disposition: attachment; filename=\"".((int)$bucketId)."_links.csv\"");

    echo "\"id\",\"id_resource_source\",\"id_resource_target\",\"is_regular\",\"in_comment\",\"in_script\"\r\n";

    for ($i = 0; $i < $linksCount; $i++)
    {
        echo "\"".((int)$links[$i]["id"])."\",".
             "\"".((int)$links[$i]["id_resource_source"])."\",".
             "\"".((int)$links[$i]["id_resource_target"])."\",";

        $contexts = (int)$links[$i]["contexts"];

        if (($contexts & RESOURCE_LINKCONTEXT_REGULAR) == RESOURCE_LINKCONTEXT_REGULAR)
        {
            echo "\"true\"";
        }
        else
        {
            echo "\"false\"";
        }

        echo ",";

        if (($contexts & RESOURCE_LINKCONTEXT_COMMENT) == RESOURCE_LINKCONTEXT_COMMENT)
        {
            echo "\"true\"";
        }
        else
        {
            echo "\"false\"";
        }

        echo ",";

        if (($contexts & RESOURCE_LINKCONTEXT_SCRIPT) == RESOURCE_LINKCONTEXT_SCRIPT)
        {
            echo "\"true\"";
        }
        else
        {
            echo "\"false\"";
        }

        echo "\r\n";
    }
}
else
{
    http_response_code(404);
    exit(1);
}



?>

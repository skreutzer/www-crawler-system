In addition to the instructions the installation routine provides,
please make sure the directory permissions for the $/crawler/storage/
directory are properly set, to allow the server to read + write files
from it!
